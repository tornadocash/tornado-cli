import 'dotenv/config';
import { Command } from 'commander';
import { JsonRpcProvider, Provider, TransactionLike, Wallet, VoidSigner } from 'ethers';
import { getProviderOptions, TornadoWallet, TornadoVoidSigner, RelayerInfo, RelayerError, RelayerClient, fetchDataOptions, NetIdType, TovarishClient, TovarishInfo } from '@tornado/core';
export interface commonProgramOptions {
    rpc?: string;
    ethRpc?: string;
    disableTovarish?: boolean;
    accountKey?: string;
    relayer?: string;
    walletWithdrawal?: boolean;
    torPort?: number;
    token?: string;
    viewOnly?: string;
    mnemonic?: string;
    mnemonicIndex?: number;
    privateKey?: string;
    nonInteractive?: boolean;
    localRpc?: boolean;
}
export declare function promptConfirmation(nonInteractive?: boolean): Promise<void>;
export declare function getIPAddress(fetchDataOptions: fetchDataOptions): Promise<{
    ip: any;
    isTor: boolean;
}>;
export declare function getProgramOptions(options: commonProgramOptions): Promise<{
    options: commonProgramOptions;
    fetchDataOptions: fetchDataOptions;
}>;
export declare function getProgramProvider(rpcUrl: string | undefined, providerOptions: getProviderOptions): Promise<JsonRpcProvider>;
export declare function getProgramSigner({ options, provider, }: {
    options: commonProgramOptions;
    provider: Provider;
}): TornadoVoidSigner | TornadoWallet | undefined;
export declare function getProgramRelayer({ options, fetchDataOptions, netId, }: {
    options: commonProgramOptions;
    fetchDataOptions?: fetchDataOptions;
    netId: NetIdType;
}): Promise<{
    validRelayers: RelayerInfo[];
    invalidRelayers: RelayerError[];
    relayerClient: RelayerClient;
}>;
export declare function getTovarishRelayer({ options, fetchDataOptions, netId, }: {
    options: commonProgramOptions;
    fetchDataOptions?: fetchDataOptions;
    netId: NetIdType;
}): Promise<{
    validRelayers: TovarishInfo[];
    invalidRelayers: RelayerError[];
    relayerClient: TovarishClient;
}>;
export declare function programSendTransaction({ signer, options, populatedTransaction, }: {
    signer: VoidSigner | Wallet;
    options: commonProgramOptions;
    populatedTransaction: TransactionLike;
}): Promise<import("ethers").TransactionResponse | undefined>;
export declare function tornadoProgram(): Command;
