import { BaseEvents, CachedEvents, MinimalEvents } from '@tornado/core';
export declare function existsAsync(fileOrDir: string): Promise<boolean>;
/**
 * Supports legacy gz format for legacy UI
 */
export declare function deflateAsync(data: Uint8Array): Promise<Buffer>;
export declare function saveLegacyFile({ fileName, userDirectory, dataString, }: {
    fileName: string;
    userDirectory: string;
    dataString: string;
}): Promise<void>;
export declare function saveUserFile({ fileName, userDirectory, dataString, }: {
    fileName: string;
    userDirectory: string;
    dataString: string;
}): Promise<void>;
export declare function loadSavedEvents<T extends MinimalEvents>({ name, userDirectory, }: {
    name: string;
    userDirectory: string;
}): Promise<BaseEvents<T>>;
export declare function download({ name, cacheDirectory }: {
    name: string;
    cacheDirectory: string;
}): Promise<string>;
export declare function loadCachedEvents<T extends MinimalEvents>({ name, cacheDirectory, deployedBlock, }: {
    name: string;
    cacheDirectory: string;
    deployedBlock: number;
}): Promise<CachedEvents<T>>;
