import { BatchBlockOnProgress, BatchEventOnProgress, BaseTornadoService, BaseEncryptedNotesService, BaseGovernanceService, BaseRegistryService, BaseRevenueService, BaseTornadoServiceConstructor, BaseEncryptedNotesServiceConstructor, BaseGovernanceServiceConstructor, BaseRegistryServiceConstructor, BaseRevenueServiceConstructor, BaseEchoServiceConstructor, BaseEchoService, CachedRelayers, BaseEvents, DepositsEvents, WithdrawalsEvents, EncryptedNotesEvents, AllRelayerRegistryEvents, AllGovernanceEvents, EchoEvents, StakeBurnedEvents } from '@tornado/core';
import { TreeCache } from './treeCache';
export type NodeServiceConstructor = {
    cacheDirectory: string;
    userDirectory: string;
};
export type NodeTornadoServiceConstructor = BaseTornadoServiceConstructor & NodeServiceConstructor & {
    nativeCurrency: string;
    treeCache?: TreeCache;
};
export declare class NodeTornadoService extends BaseTornadoService {
    cacheDirectory: string;
    userDirectory: string;
    nativeCurrency: string;
    treeCache?: TreeCache;
    constructor(serviceConstructor: NodeTornadoServiceConstructor);
    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]): void;
    updateTransactionProgress({ currentIndex, totalIndex }: Parameters<BatchBlockOnProgress>[0]): void;
    updateBlockProgress({ currentIndex, totalIndex }: Parameters<BatchBlockOnProgress>[0]): void;
    getEventsFromDB(): Promise<BaseEvents<DepositsEvents | WithdrawalsEvents>>;
    getEventsFromCache(): Promise<import("@tornado/core").CachedEvents<DepositsEvents | WithdrawalsEvents>>;
    validateEvents<S>({ events, lastBlock, hasNewEvents, }: BaseEvents<DepositsEvents | WithdrawalsEvents> & {
        hasNewEvents?: boolean;
    }): Promise<S>;
    saveEvents({ events, lastBlock }: BaseEvents<DepositsEvents | WithdrawalsEvents>): Promise<void>;
}
export type NodeEchoServiceConstructor = BaseEchoServiceConstructor & NodeServiceConstructor;
export declare class NodeEchoService extends BaseEchoService {
    cacheDirectory: string;
    userDirectory: string;
    constructor(serviceConstructor: NodeEchoServiceConstructor);
    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]): void;
    getEventsFromDB(): Promise<BaseEvents<EchoEvents>>;
    getEventsFromCache(): Promise<import("@tornado/core").CachedEvents<EchoEvents>>;
    saveEvents({ events, lastBlock }: BaseEvents<EchoEvents>): Promise<void>;
}
export type NodeEncryptedNotesServiceConstructor = BaseEncryptedNotesServiceConstructor & NodeServiceConstructor;
export declare class NodeEncryptedNotesService extends BaseEncryptedNotesService {
    cacheDirectory: string;
    userDirectory: string;
    constructor(serviceConstructor: NodeEncryptedNotesServiceConstructor);
    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]): void;
    getEventsFromDB(): Promise<BaseEvents<EncryptedNotesEvents>>;
    getEventsFromCache(): Promise<import("@tornado/core").CachedEvents<EncryptedNotesEvents>>;
    saveEvents({ events, lastBlock }: BaseEvents<EncryptedNotesEvents>): Promise<void>;
}
export type NodeGovernanceServiceConstructor = BaseGovernanceServiceConstructor & NodeServiceConstructor;
export declare class NodeGovernanceService extends BaseGovernanceService {
    cacheDirectory: string;
    userDirectory: string;
    constructor(serviceConstructor: NodeGovernanceServiceConstructor);
    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]): void;
    updateTransactionProgress({ currentIndex, totalIndex }: Parameters<BatchBlockOnProgress>[0]): void;
    getEventsFromDB(): Promise<BaseEvents<AllGovernanceEvents>>;
    getEventsFromCache(): Promise<import("@tornado/core").CachedEvents<AllGovernanceEvents>>;
    saveEvents({ events, lastBlock }: BaseEvents<AllGovernanceEvents>): Promise<void>;
}
export type NodeRegistryServiceConstructor = BaseRegistryServiceConstructor & NodeServiceConstructor;
export declare class NodeRegistryService extends BaseRegistryService {
    cacheDirectory: string;
    userDirectory: string;
    constructor(serviceConstructor: NodeRegistryServiceConstructor);
    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]): void;
    getEventsFromDB(): Promise<BaseEvents<AllRelayerRegistryEvents>>;
    getEventsFromCache(): Promise<import("@tornado/core").CachedEvents<AllRelayerRegistryEvents>>;
    saveEvents({ events, lastBlock }: BaseEvents<AllRelayerRegistryEvents>): Promise<void>;
    getRelayersFromDB(): Promise<CachedRelayers>;
    getRelayersFromCache(): Promise<CachedRelayers>;
    saveRelayers({ lastBlock, timestamp, relayers }: CachedRelayers): Promise<void>;
}
export type NodeRevenueServiceConstructor = BaseRevenueServiceConstructor & NodeServiceConstructor;
export declare class NodeRevenueService extends BaseRevenueService {
    cacheDirectory: string;
    userDirectory: string;
    constructor(serviceConstructor: NodeRevenueServiceConstructor);
    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]): void;
    updateTransactionProgress({ currentIndex, totalIndex }: Parameters<BatchBlockOnProgress>[0]): void;
    updateBlockProgress({ currentIndex, totalIndex }: Parameters<BatchBlockOnProgress>[0]): void;
    getEventsFromDB(): Promise<BaseEvents<StakeBurnedEvents>>;
    getEventsFromCache(): Promise<import("@tornado/core").CachedEvents<StakeBurnedEvents>>;
    saveEvents({ events, lastBlock }: BaseEvents<StakeBurnedEvents>): Promise<void>;
}
