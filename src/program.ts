import 'dotenv/config';
import { readFile, writeFile } from 'fs/promises';
import path from 'path';
import process from 'process';
import { createInterface } from 'readline';
import { Command } from 'commander';
import Table from 'cli-table3';
import colors from '@colors/colors';
import moment from 'moment';
import {
    Tornado__factory,
    TornadoRouter__factory,
    RelayerRegistry__factory,
    Aggregator__factory,
    Governance__factory,
    Echoer__factory,
    TORN__factory,
    GovernanceVaultUpgrade__factory,
} from '@tornado/contracts';
import {
    JsonRpcProvider,
    Provider,
    TransactionLike,
    Wallet,
    VoidSigner,
    formatEther,
    formatUnits,
    parseEther,
    parseUnits,
    ZeroAddress,
    MaxUint256,
    Transaction,
    getAddress,
} from 'ethers';
import {
    ERC20,
    ERC20__factory,
    Multicall__factory,
    OffchainOracle__factory,
    OvmGasPriceOracle__factory,
    getProviderOptions,
    getProvider,
    getTokenBalances,
    TornadoWallet,
    TornadoVoidSigner,
    tokenBalances,
    Deposit,
    DepositsEvents,
    RelayerInfo,
    RelayerError,
    RelayerClient,
    WithdrawalsEvents,
    TornadoFeeOracle,
    TokenPriceOracle,
    calculateSnarkProof,
    MerkleTreeService,
    multicall,
    Invoice,
    fetchData,
    fetchDataOptions,
    NetId,
    NetIdType,
    getInstanceByAddress,
    getConfig,
    enabledChains,
    substring,
    NoteAccount,
    getSupportedInstances,
    initGroth16,
    getRelayerEnsSubdomains,
    getActiveTokens,
    TovarishClient,
    TovarishInfo,
    ReverseRecords__factory,
    numberFormatter,
} from '@tornado/core';
import type { MerkleTree } from '@tornado/fixed-merkle-tree';
import * as packageJson from '../package.json';
import {
    parseUrl,
    parseRelayer,
    parseNumber,
    parseMnemonic,
    parseKey,
    parseAddress,
    parseRecoveryKey,
    NodeTornadoService,
    NodeRegistryService,
    NodeEchoService,
    NodeEncryptedNotesService,
    NodeGovernanceService,
    TreeCache,
    NodeRevenueService,
} from './services';

const EXEC_NAME = 'tornado-cli';

/**
 * Static variables, shouldn't be modified by env unless you know what they are doing
 */
const DEFAULT_GAS_LIMIT = Number(process.env.DEFAULT_GAS_LIMIT) || 600_000;

const RELAYER_NETWORK = Number(process.env.RELAYER_NETWORK) || NetId.MAINNET;

// Where cached events, trees, circuits, and key is saved
const STATIC_DIR = process.env.CACHE_DIR || path.join(__dirname, '../static');
const EVENTS_DIR = path.join(STATIC_DIR, './events');
const merkleWorkerPath = path.join(STATIC_DIR, './merkleTreeWorker.js');

// Where we should backup notes and save events
const USER_DIR = process.env.USER_DIR || '.';
const SAVED_DIR = path.join(USER_DIR, './events');
const SAVED_TREE_DIR = path.join(USER_DIR, './trees');

const CIRCUIT_PATH = path.join(__dirname, '../static/tornado.json');
const KEY_PATH = path.join(__dirname, '../static/tornadoProvingKey.bin');

interface packageJson {
    name: string;
    version: string;
    description: string;
}

export interface commonProgramOptions {
    rpc?: string;
    ethRpc?: string;
    disableTovarish?: boolean;
    accountKey?: string;
    relayer?: string;
    walletWithdrawal?: boolean;
    torPort?: number;
    token?: string;
    viewOnly?: string;
    mnemonic?: string;
    mnemonicIndex?: number;
    privateKey?: string;
    nonInteractive?: boolean;
    localRpc?: boolean;
}

export async function promptConfirmation(nonInteractive?: boolean) {
    if (nonInteractive) {
        return;
    }

    const prompt = createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    const query = 'Confirm? [Y/n]\n';

    const confirmation = await new Promise((resolve) => prompt.question(query, resolve));

    if (!confirmation || (confirmation as string).toUpperCase() !== 'Y') {
        throw new Error('User canceled');
    }
}

export async function getIPAddress(fetchDataOptions: fetchDataOptions) {
    const htmlIPInfo = await fetchData('https://check.torproject.org', {
        ...fetchDataOptions,
        method: 'GET',
        headers: {
            'Content-Type': 'text/html; charset=utf-8',
        },
    });
    const ip = htmlIPInfo.split('Your IP address appears to be:  <strong>').pop().split('</')[0];
    const isTor = htmlIPInfo.includes('Sorry. You are not using Tor.') ? false : true;

    return {
        ip,
        isTor,
    };
}

export async function getProgramOptions(options: commonProgramOptions): Promise<{
    options: commonProgramOptions;
    fetchDataOptions: fetchDataOptions;
}> {
    options = {
        rpc: options.rpc || (process.env.RPC_URL ? parseUrl(process.env.RPC_URL) : undefined),
        ethRpc: options.ethRpc || (process.env.ETHRPC_URL ? parseUrl(process.env.ETHRPC_URL) : undefined),
        disableTovarish:
            Boolean(options.disableTovarish) || (process.env.DISABLE_TOVARISH === 'true' ? true : undefined),
        accountKey:
            options.accountKey || (process.env.ACCOUNT_KEY ? parseRecoveryKey(process.env.ACCOUNT_KEY) : undefined),
        relayer: options.relayer || (process.env.RELAYER ? parseRelayer(process.env.RELAYER) : undefined),
        walletWithdrawal:
            Boolean(options.walletWithdrawal) || (process.env.WALLET_WITHDRAWAL === 'true' ? true : undefined),
        torPort: options.torPort || (process.env.TOR_PORT ? parseNumber(process.env.TOR_PORT) : undefined),
        token: options.token || (process.env.TOKEN ? parseAddress(process.env.TOKEN) : undefined),
        viewOnly: options.viewOnly || (process.env.VIEW_ONLY ? parseAddress(process.env.VIEW_ONLY) : undefined),
        mnemonic: options.mnemonic || (process.env.MNEMONIC ? parseMnemonic(process.env.MNEMONIC) : undefined),
        mnemonicIndex:
            options.mnemonicIndex || (process.env.MNEMONIC_INDEX ? parseNumber(process.env.MNEMONIC_INDEX) : undefined),
        privateKey: options.privateKey || (process.env.PRIVATE_KEY ? parseKey(process.env.PRIVATE_KEY) : undefined),
        nonInteractive: Boolean(options.nonInteractive) || (process.env.NON_INTERACTIVE === 'true' ? true : undefined),
        localRpc: Boolean(options.localRpc) || (process.env.LOCAL_RPC === 'true' ? true : undefined),
    };

    const fetchDataOptions = {
        torPort: options.torPort,
    };

    const { ip, isTor } = await getIPAddress(fetchDataOptions);

    const optionsTable = new Table();

    optionsTable.push(
        [{ colSpan: 2, content: 'Program Options', hAlign: 'center' }],
        ['IP', ip],
        ['Is Tor', isTor],
        ...(Object.entries(options)
            .map(([key, value]) => {
                if (typeof value !== 'undefined') {
                    return [key, value];
                }
            })
            .filter((r) => r) as string[][]),
    );

    console.log('\n' + optionsTable.toString() + '\n');

    await promptConfirmation(options.nonInteractive);

    return {
        options,
        fetchDataOptions,
    };
}

export async function getProgramProvider(
    rpcUrl: string = '',
    providerOptions: getProviderOptions,
): Promise<JsonRpcProvider> {
    const { netId } = providerOptions;

    const config = getConfig(netId);

    if (!rpcUrl) {
        rpcUrl = Object.values(config.rpcUrls)[0]?.url || '';
    }

    const provider = await getProvider(rpcUrl, providerOptions);

    console.log(`\nConnected with the ${config.networkName} RPC: ${provider._getConnection().url}\n`);

    return provider;
}

export function getProgramSigner({
    options,
    provider,
}: {
    options: commonProgramOptions;
    provider: Provider;
}): TornadoVoidSigner | TornadoWallet | undefined {
    if (options.viewOnly) {
        return new TornadoVoidSigner(options.viewOnly, provider);
    }

    if (options.privateKey) {
        return new TornadoWallet(options.privateKey, provider);
    }

    if (options.mnemonic) {
        return TornadoWallet.fromMnemonic(options.mnemonic, provider, options.mnemonicIndex);
    }
}

export async function getProgramRelayer({
    options,
    fetchDataOptions,
    netId,
}: {
    options: commonProgramOptions;
    fetchDataOptions?: fetchDataOptions;
    netId: NetIdType;
}): Promise<{
    validRelayers: RelayerInfo[];
    invalidRelayers: RelayerError[];
    relayerClient: RelayerClient;
}> {
    const { rpc, ethRpc, relayer } = options;

    const netConfig = getConfig(netId);

    const ethConfig = getConfig(RELAYER_NETWORK);

    const {
        aggregatorContract,
        registryContract,
        constants: { REGISTRY_BLOCK },
    } = ethConfig;

    const rpcUrl = netId === RELAYER_NETWORK ? rpc : ethRpc;

    const provider = await getProgramProvider(rpcUrl, {
        netId: RELAYER_NETWORK,
        ...fetchDataOptions,
    });

    const registryService = new NodeRegistryService({
        netId: RELAYER_NETWORK,
        provider,
        RelayerRegistry: RelayerRegistry__factory.connect(registryContract as string, provider),
        Aggregator: Aggregator__factory.connect(aggregatorContract as string, provider),
        relayerEnsSubdomains: getRelayerEnsSubdomains(),
        deployedBlock: REGISTRY_BLOCK,
        fetchDataOptions,
        cacheDirectory: EVENTS_DIR,
        userDirectory: SAVED_DIR,
    });

    const relayerClient = new RelayerClient({
        netId,
        config: netConfig,
        fetchDataOptions,
    });

    // If user provided relayer is a full working URL
    if (relayer && relayer.startsWith('http')) {
        const relayerStatus = await relayerClient.askRelayerStatus({
            url: relayer,
        });

        relayerClient.selectedRelayer = {
            ensName: new URL(relayerStatus.url).hostname,
            relayerAddress: getAddress(relayerStatus.rewardAccount),
            netId: relayerStatus.netId,
            url: relayerStatus.url,
            hostname: new URL(relayerStatus.url).hostname,
            rewardAccount: getAddress(relayerStatus.rewardAccount),
            instances: getSupportedInstances(relayerStatus.instances),
            gasPrice: relayerStatus.gasPrices?.fast,
            ethPrices: relayerStatus.ethPrices,
            currentQueue: relayerStatus.currentQueue,
            tornadoServiceFee: relayerStatus.tornadoServiceFee,
        };

        return {
            validRelayers: [relayerClient.selectedRelayer],
            invalidRelayers: [],
            relayerClient,
        };
    }

    console.log('\nGetting valid relayers from registry, would take some time\n');

    const { validRelayers, invalidRelayers } = await relayerClient.getValidRelayers(
        (await registryService.updateRelayers()).relayers,
    );

    // If user has provided either a hostname or ensname of known registered relayer
    const foundRelayer = relayer
        ? validRelayers.find((r) => r.hostname.includes(relayer) || r.ensName.includes(relayer))
        : undefined;

    if (foundRelayer) {
        relayerClient.selectedRelayer = foundRelayer;

        return {
            validRelayers: [foundRelayer],
            invalidRelayers: [],
            relayerClient,
        };
    }

    const relayerStatus = relayerClient.pickWeightedRandomRelayer(validRelayers);

    if (relayerStatus) {
        relayerClient.selectedRelayer = relayerStatus;
    }

    return {
        validRelayers,
        invalidRelayers,
        relayerClient,
    };
}

export async function getTovarishRelayer({
    options,
    fetchDataOptions,
    netId,
}: {
    options: commonProgramOptions;
    fetchDataOptions?: fetchDataOptions;
    netId: NetIdType;
}): Promise<{
    validRelayers: TovarishInfo[];
    invalidRelayers: RelayerError[];
    relayerClient: TovarishClient;
}> {
    const { rpc, ethRpc, relayer } = options;

    const netConfig = getConfig(netId);

    const ethConfig = getConfig(RELAYER_NETWORK);

    const {
        aggregatorContract,
        registryContract,
        constants: { REGISTRY_BLOCK },
    } = ethConfig;

    const rpcUrl = netId === RELAYER_NETWORK ? rpc : ethRpc;

    const provider = await getProgramProvider(rpcUrl, {
        netId: RELAYER_NETWORK,
        ...fetchDataOptions,
    });

    const registryService = new NodeRegistryService({
        netId: RELAYER_NETWORK,
        provider,
        RelayerRegistry: RelayerRegistry__factory.connect(registryContract as string, provider),
        Aggregator: Aggregator__factory.connect(aggregatorContract as string, provider),
        relayerEnsSubdomains: getRelayerEnsSubdomains(),
        deployedBlock: REGISTRY_BLOCK,
        fetchDataOptions,
        cacheDirectory: EVENTS_DIR,
        userDirectory: SAVED_DIR,
    });

    const relayerClient = new TovarishClient({
        netId,
        config: netConfig,
        fetchDataOptions,
    });

    // If user provided relayer is a full working URL
    if (relayer && relayer.startsWith('http')) {
        const relayerStatus = await relayerClient.askRelayerStatus({
            url: relayer,
        });

        relayerClient.selectedRelayer = {
            ensName: new URL(relayerStatus.url).hostname,
            relayerAddress: getAddress(relayerStatus.rewardAccount),
            netId: relayerStatus.netId,
            url: relayerStatus.url,
            hostname: new URL(relayerStatus.url).hostname,
            rewardAccount: getAddress(relayerStatus.rewardAccount),
            instances: getSupportedInstances(relayerStatus.instances),
            gasPrice: relayerStatus.gasPrices?.fast,
            ethPrices: relayerStatus.ethPrices,
            currentQueue: relayerStatus.currentQueue,
            tornadoServiceFee: relayerStatus.tornadoServiceFee,
            latestBlock: Number(relayerStatus.latestBlock),
            latestBalance: relayerStatus.latestBalance,
            version: relayerStatus.version,
            events: relayerStatus.events,
            syncStatus: relayerStatus.syncStatus,
        };

        return {
            validRelayers: [relayerClient.selectedRelayer],
            invalidRelayers: [],
            relayerClient,
        };
    }

    console.log('\nGetting valid tovarish relayers\n');

    const { validRelayers, invalidRelayers } = await relayerClient.getValidRelayers(
        (await registryService.getRelayersFromCache()).relayers,
    );

    // If user has provided either a hostname or ensname of known registered relayer
    const foundRelayer = relayer
        ? validRelayers.find((r) => r.hostname.includes(relayer) || r.ensName.includes(relayer))
        : undefined;

    if (foundRelayer) {
        relayerClient.selectedRelayer = foundRelayer;

        return {
            validRelayers: [foundRelayer],
            invalidRelayers: [],
            relayerClient,
        };
    }

    if (validRelayers.length) {
        relayerClient.selectedRelayer = validRelayers[0];
    } else {
        const errMsg = `Working Tovarish Relayer for net ${netId} not found, you can try with legacy relayer with DISABLE_TOVARISH='true' env and try again`;
        throw new Error(errMsg);
    }

    return {
        validRelayers,
        invalidRelayers,
        relayerClient,
    };
}

export async function programSendTransaction({
    signer,
    options,
    populatedTransaction,
}: {
    signer: VoidSigner | Wallet;
    options: commonProgramOptions;
    populatedTransaction: TransactionLike;
}) {
    const txTable = new Table();
    // ethers.js doesn't output complete transaction from the contract runner so we populate the transaction once again
    const txObject =
        !populatedTransaction.gasLimit || !populatedTransaction.from
            ? (JSON.parse(JSON.stringify(await signer.populateTransaction(populatedTransaction))) as TransactionLike)
            : (JSON.parse(JSON.stringify(populatedTransaction, null, 2)) as TransactionLike);

    const txType =
        signer instanceof VoidSigner
            ? 'Unsigned Transaction'
            : options.localRpc
              ? 'Unbroadcasted Transaction'
              : 'Send Transaction?';

    txTable.push(
        [{ colSpan: 2, content: txType, hAlign: 'center' }],
        ...Object.entries(txObject).map(([key, value]) => {
            if (key === 'data' && value) {
                return ['data', substring(value, 40)];
            }
            return [key, value];
        }),
    );

    if (txType === 'Unsigned Transaction') {
        // delete from field as the Transaction.from method doesn't accept it from unsigned tx
        delete txObject.from;
        const transaction = Transaction.from(txObject);
        console.log('\n' + txTable.toString() + '\n');
        console.log('Sign this transaction:\n');
        console.log(`${transaction.unsignedSerialized}\n`);
        return;
    }

    if (txType === 'Unbroadcasted Transaction') {
        const signedTx = await signer.signTransaction(txObject);
        console.log('\n' + txTable.toString() + '\n');
        console.log('Broadcast transaction:\n');
        console.log(`${signedTx}\n`);
        return;
    }

    console.log('\n' + txTable.toString() + '\n');

    await promptConfirmation(options.nonInteractive);

    const resp = await signer.sendTransaction(txObject);

    console.log(`Sent transaction ${resp.hash}\n`);

    return resp;
}

export function tornadoProgram() {
    const { version, description } = packageJson as packageJson;

    const program = new Command();

    program.name(EXEC_NAME).version(version).description(description);

    program
        .command('create')
        .description('Creates Tornado Cash deposit note and deposit invoice')
        .argument('<netId>', 'Network Chain ID to connect with (see https://chainlist.org for examples)', parseNumber)
        .argument('<currency>', 'Currency to deposit on Tornado Cash')
        .argument('<amount>', 'Amount to deposit on Tornado Cash')
        .action(async (netId: NetIdType, currency: string, amount: string) => {
            currency = currency.toLowerCase();

            const config = getConfig(netId);

            const {
                routerContract,
                nativeCurrency,
                tokens: { [currency]: currencyConfig },
            } = config;

            const {
                decimals,
                tokenAddress,
                instanceAddress: { [amount]: instanceAddress },
            } = currencyConfig;

            const isEth = nativeCurrency === currency;
            const denomination = parseUnits(amount, decimals);

            const deposit = await Deposit.createNote({
                currency,
                amount,
                netId,
            });

            const { noteHex, note, commitmentHex } = deposit;

            const ERC20Interface = ERC20__factory.createInterface();
            const TornadoRouterInterface = TornadoRouter__factory.createInterface();

            console.log(`New deposit: ${deposit.toString()}\n`);

            await writeFile(`./backup-tornado-${currency}-${amount}-${netId}-${noteHex.slice(0, 10)}.txt`, note, {
                encoding: 'utf8',
            });

            const depositData = TornadoRouterInterface.encodeFunctionData('deposit', [
                instanceAddress,
                commitmentHex,
                '0x',
            ]);

            if (!isEth) {
                const approveData = ERC20Interface.encodeFunctionData('approve', [routerContract, MaxUint256]);

                console.log(`Approve Data: ${JSON.stringify({ to: tokenAddress, data: approveData }, null, 2)}]\n`);

                console.log(
                    `Transaction Data: ${JSON.stringify({ to: routerContract, data: depositData }, null, 2)}\n`,
                );
            } else {
                console.log(
                    `Transaction Data: ${JSON.stringify({ to: routerContract, value: denomination.toString(), data: depositData }, null, 2)}`,
                );
            }

            process.exit(0);
        });

    program
        .command('deposit')
        .description(
            'Submit a deposit of specified currency and amount from default eth account and return the resulting note. \n\n' +
                'The currency is one of (ETH|DAI|cDAI|USDC|cUSDC|USDT). \n\n' +
                'The amount depends on currency, see config.js file or see Tornado Cash UI.',
        )
        .argument('<netId>', 'Network Chain ID to connect with (see https://chainlist.org for examples)', parseNumber)
        .argument('<currency>', 'Currency to deposit on Tornado Cash')
        .argument('<amount>', 'Amount to deposit on Tornado Cash')
        .action(async (netId: NetIdType, currency: string, amount: string, cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
            currency = currency.toLowerCase();
            const { rpc, accountKey } = options;

            const config = getConfig(netId);

            const {
                multicallContract,
                routerContract,
                nativeCurrency,
                tokens: { [currency]: currencyConfig },
            } = config;

            const {
                decimals,
                tokenAddress,
                instanceAddress: { [amount]: instanceAddress },
            } = currencyConfig;

            const isEth = nativeCurrency === currency;
            const denomination = parseUnits(amount, decimals);

            const provider = await getProgramProvider(rpc, {
                netId,
                ...fetchDataOptions,
            });

            const signer = getProgramSigner({
                options,
                provider,
            });

            const noteAccount = accountKey ? new NoteAccount({ recoveryKey: accountKey }) : undefined;

            if (!signer) {
                throw new Error(
                    'Signer not defined, make sure you have either viewOnly address, mnemonic, or private key configured',
                );
            }

            const TornadoProxy = TornadoRouter__factory.connect(routerContract, signer);
            const Multicall = Multicall__factory.connect(multicallContract, provider);
            const Token = tokenAddress ? ERC20__factory.connect(tokenAddress, signer) : undefined;

            const [ethBalance, tokenBalance, tokenApprovals] = await multicall(Multicall, [
                {
                    contract: Multicall,
                    name: 'getEthBalance',
                    params: [signer.address],
                },
                ...(!isEth
                    ? [
                          {
                              contract: Token as ERC20,
                              name: 'balanceOf',
                              params: [signer.address],
                          },
                          {
                              contract: Token as ERC20,
                              name: 'allowance',
                              params: [signer.address, routerContract],
                          },
                      ]
                    : []),
            ]);

            if (isEth && denomination > ethBalance) {
                const errMsg = `Invalid ${currency.toUpperCase()} balance, wants ${amount} have ${formatUnits(ethBalance, decimals)}`;
                throw new Error(errMsg);
            } else if (!isEth && denomination > tokenBalance) {
                const errMsg = `Invalid ${currency.toUpperCase()} balance, wants ${amount} have ${formatUnits(tokenBalance, decimals)}`;
                throw new Error(errMsg);
            }

            if (!isEth && denomination > tokenApprovals) {
                // token approval
                const resp = await programSendTransaction({
                    signer,
                    options,
                    populatedTransaction: await (Token as ERC20).approve.populateTransaction(
                        routerContract,
                        MaxUint256,
                    ),
                });

                // wait until signer sends the approve transaction offline
                if (signer instanceof VoidSigner || options.localRpc) {
                    console.log(
                        'Signer can not sign or broadcast transactions, please send the token approve transaction first and try again.\n',
                    );
                    process.exit(0);
                } else {
                    // Wait until the approval tx is confirmed
                    await resp?.wait();
                }
            }

            const deposit = await Deposit.createNote({
                currency,
                amount,
                netId,
            });

            const { note, noteHex, commitmentHex } = deposit;

            const encryptedNote = noteAccount
                ? noteAccount.encryptNote({
                      address: instanceAddress,
                      noteHex,
                  })
                : '0x';

            const backupFile = `./backup-tornado-${currency}-${amount}-${netId}-${noteHex.slice(0, 10)}.txt`;

            console.log(`New deposit: ${deposit.toString()}\n`);

            console.log(`Writing note backup at ${backupFile}\n`);

            await writeFile(backupFile, note, { encoding: 'utf8' });

            if (encryptedNote !== '0x') {
                console.log(`Storing encrypted note on-chain for backup (Account key: ${accountKey})\n`);
            }

            await programSendTransaction({
                signer,
                options,
                populatedTransaction: await TornadoProxy.deposit.populateTransaction(
                    instanceAddress,
                    commitmentHex,
                    encryptedNote,
                    {
                        value: isEth ? denomination : BigInt(0),
                    },
                ),
            });

            process.exit(0);
        });

    program
        .command('depositInvoice')
        .description(
            'Submit a deposit of tornado invoice from default eth account and return the resulting note. \n\n' +
                'Useful to deposit on online computer without exposing note',
        )
        .argument('<invoice>', 'Tornado Cash Invoice generated from create command')
        .action(async (invoiceString: string, cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
            const { rpc } = options;

            const { currency, amount, netId, commitmentHex } = new Invoice(invoiceString);

            const config = getConfig(netId);

            const {
                multicallContract,
                routerContract,
                nativeCurrency,
                tokens: { [currency]: currencyConfig },
            } = config;

            const {
                decimals,
                tokenAddress,
                instanceAddress: { [amount]: instanceAddress },
            } = currencyConfig;

            const isEth = nativeCurrency === currency;
            const denomination = parseUnits(amount, decimals);

            const provider = await getProgramProvider(rpc, {
                netId,
                ...fetchDataOptions,
            });

            const signer = getProgramSigner({
                options,
                provider,
            });

            if (!signer) {
                throw new Error(
                    'Signer not defined, make sure you have either viewOnly address, mnemonic, or private key configured',
                );
            }

            const TornadoProxy = TornadoRouter__factory.connect(routerContract, signer);
            const Multicall = Multicall__factory.connect(multicallContract, provider);
            const Token = tokenAddress ? ERC20__factory.connect(tokenAddress, signer) : undefined;

            const [ethBalance, tokenBalance, tokenApprovals] = await multicall(Multicall, [
                {
                    contract: Multicall,
                    name: 'getEthBalance',
                    params: [signer.address],
                },
                ...(!isEth
                    ? [
                          {
                              contract: Token as ERC20,
                              name: 'balanceOf',
                              params: [signer.address],
                          },
                          {
                              contract: Token as ERC20,
                              name: 'allowance',
                              params: [signer.address, routerContract],
                          },
                      ]
                    : []),
            ]);

            if (isEth && denomination > ethBalance) {
                const errMsg = `Invalid ${currency.toUpperCase()} balance, wants ${amount} have ${formatUnits(ethBalance, decimals)}`;
                throw new Error(errMsg);
            } else if (!isEth && denomination > tokenBalance) {
                const errMsg = `Invalid ${currency.toUpperCase()} balance, wants ${amount} have ${formatUnits(tokenBalance, decimals)}`;
                throw new Error(errMsg);
            }

            if (!isEth && denomination > tokenApprovals) {
                // token approval
                await programSendTransaction({
                    signer,
                    options,
                    populatedTransaction: await (Token as ERC20).approve.populateTransaction(
                        routerContract,
                        MaxUint256,
                    ),
                });

                // wait until signer sends the approve transaction offline
                if (signer instanceof VoidSigner) {
                    console.log(
                        'Signer can not sign transactions, please send the token approve transaction first and try again.\n',
                    );
                    process.exit(0);
                }
            }

            await programSendTransaction({
                signer,
                options,
                populatedTransaction: await TornadoProxy.deposit.populateTransaction(
                    instanceAddress,
                    commitmentHex,
                    '0x',
                    {
                        value: isEth ? denomination : BigInt(0),
                    },
                ),
            });

            process.exit(0);
        });

    program
        .command('withdraw')
        .description(
            'Withdraw a note to a recipient account using relayer or specified private key. \n\n' +
                'You can exchange some of your deposit`s tokens to ETH during the withdrawal by ' +
                'specifing ETH_purchase (e.g. 0.01) to pay for gas in future transactions. \n\n' +
                'Also see the --relayer option.\n\n',
        )
        .argument('<note>', 'Tornado Cash Deposit Note')
        .argument('<recipient>', 'Recipient to receive withdrawn amount', parseAddress)
        .argument('[ETH_purchase]', 'ETH to purchase', parseNumber)
        .action(
            async (
                note: string,
                recipient: string,
                ethPurchase: number | undefined,
                cmdOptions: commonProgramOptions,
            ) => {
                const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
                const { rpc, walletWithdrawal, disableTovarish } = options;

                // Prepare groth16 in advance
                initGroth16();

                const deposit = await Deposit.parseNote(note);

                const { netId, currency, amount, commitmentHex, nullifierHex, nullifier, secret } = deposit;

                const config = getConfig(netId);

                const {
                    deployedBlock,
                    nativeCurrency,
                    multicallContract,
                    routerContract,
                    offchainOracleContract,
                    ovmGasPriceOracleContract,
                    tokens: { [currency]: currencyConfig },
                } = config;

                const {
                    decimals,
                    tokenAddress,
                    gasLimit: instanceGasLimit,
                    tokenGasLimit,
                    instanceAddress: { [amount]: instanceAddress },
                } = currencyConfig;

                const isEth = nativeCurrency === currency;
                const denomination = parseUnits(amount, decimals);
                const firstAmount = Object.keys(currencyConfig.instanceAddress).sort(
                    (a, b) => Number(a) - Number(b),
                )[0];
                const isFirstAmount = Number(amount) === Number(firstAmount);

                const provider = await getProgramProvider(rpc, {
                    netId,
                    ...fetchDataOptions,
                });

                const { relayerClient } = disableTovarish
                    ? await getProgramRelayer({
                          options,
                          fetchDataOptions,
                          netId,
                      })
                    : await getTovarishRelayer({
                          options,
                          fetchDataOptions,
                          netId,
                      });

                const tovarishClient = relayerClient.tovarish ? (relayerClient as TovarishClient) : undefined;

                if (tovarishClient?.selectedRelayer) {
                    console.log(`\nConnected with Tovarish Relayer ${tovarishClient.selectedRelayer.url}\n`);
                }

                if (!walletWithdrawal && !relayerClient?.selectedRelayer) {
                    throw new Error(
                        'No valid relayer found for the network, you can either try again, or find any relayers using the relayers command and set with --relayer option',
                    );
                }

                const signer = getProgramSigner({
                    options,
                    provider,
                });
                const noSigner = Boolean(!signer || signer instanceof VoidSigner);

                if (walletWithdrawal && noSigner) {
                    throw new Error('Wallet withdrawal is configured however could not find any wallets');
                }

                const Tornado = Tornado__factory.connect(instanceAddress, provider);
                const TornadoProxy = TornadoRouter__factory.connect(
                    routerContract,
                    !walletWithdrawal ? provider : signer,
                );
                const Multicall = Multicall__factory.connect(multicallContract, provider);

                const tornadoFeeOracle = new TornadoFeeOracle(
                    provider,
                    ovmGasPriceOracleContract
                        ? OvmGasPriceOracle__factory.connect(ovmGasPriceOracleContract, provider)
                        : undefined,
                );

                const tokenPriceOracle = new TokenPriceOracle(
                    provider,
                    Multicall,
                    offchainOracleContract
                        ? OffchainOracle__factory.connect(offchainOracleContract, provider)
                        : undefined,
                );

                const TornadoServiceConstructor = {
                    netId,
                    provider,
                    Tornado,
                    amount,
                    currency,
                    deployedBlock,
                    fetchDataOptions,
                    tovarishClient,
                    cacheDirectory: EVENTS_DIR,
                    userDirectory: SAVED_DIR,
                    nativeCurrency,
                };

                const depositsService = new NodeTornadoService({
                    ...TornadoServiceConstructor,
                    type: 'Deposit',
                    merkleTreeService: new MerkleTreeService({
                        netId,
                        amount,
                        currency,
                        Tornado,
                        merkleWorkerPath,
                    }),
                });

                const withdrawalsService = new NodeTornadoService({
                    ...TornadoServiceConstructor,
                    type: 'Withdrawal',
                });

                const { events: depositEvents, validateResult: tree } =
                    await depositsService.updateEvents<MerkleTree>();

                const withdrawalEvents = (await withdrawalsService.updateEvents()).events as WithdrawalsEvents[];

                const depositEvent = (depositEvents as DepositsEvents[]).find(
                    ({ commitment }) => commitment === commitmentHex,
                );

                const withdrawalEvent = withdrawalEvents.find(({ nullifierHash }) => nullifierHash === nullifierHex);

                if (!depositEvent) {
                    throw new Error('Deposit not found');
                }

                const complianceTable = new Table();

                const depositDate = new Date(depositEvent.timestamp * 1000);

                complianceTable.push(
                    [{ colSpan: 2, content: 'Deposit', hAlign: 'center' }],
                    ['Deposit', `${amount} ${currency.toUpperCase()}`],
                    [
                        'Date',
                        `${depositDate.toLocaleDateString()} ${depositDate.toLocaleTimeString()} (${moment.unix(depositEvent.timestamp).fromNow()})`,
                    ],
                    ['From', depositEvent.from],
                    ['Transaction', depositEvent.transactionHash],
                    ['Commitment', commitmentHex],
                    ['Spent', Boolean(withdrawalEvent)],
                );

                if (withdrawalEvent) {
                    const withdrawalDate = new Date(withdrawalEvent.timestamp * 1000);

                    complianceTable.push(
                        [{ colSpan: 2, content: 'Withdraw', hAlign: 'center' }],
                        ['Withdrawal', `${amount} ${currency.toUpperCase()}`],
                        ['Relayer Fee', `${formatUnits(withdrawalEvent.fee, decimals)} ${currency.toUpperCase()}`],
                        [
                            'Date',
                            `${withdrawalDate.toLocaleDateString()} ${withdrawalDate.toLocaleTimeString()} (${moment.unix(withdrawalEvent.timestamp).fromNow()})`,
                        ],
                        ['To', withdrawalEvent.to],
                        ['Transaction', withdrawalEvent.transactionHash],
                        ['Nullifier', nullifierHex],
                    );
                }

                console.log('\n\n' + complianceTable.toString() + '\n');

                if (withdrawalEvent) {
                    throw new Error('Note is already spent');
                }

                const [circuit, provingKey, netGasPrice, l1Fee, tokenPriceInWei] = await Promise.all([
                    readFile(CIRCUIT_PATH, { encoding: 'utf8' }).then((s) => JSON.parse(s)),
                    readFile(KEY_PATH).then((b) => new Uint8Array(b).buffer),
                    tornadoFeeOracle.gasPrice(),
                    tornadoFeeOracle.fetchL1OptimismFee(),
                    !isEth ? tokenPriceOracle.fetchPrice(tokenAddress as string, decimals) : BigInt(0),
                ]);

                const { pathElements, pathIndices } = tree.path((depositEvent as DepositsEvents).leafIndex);

                let gasPrice = netGasPrice;

                if (!walletWithdrawal && !tovarishClient && netId === NetId.BSC) {
                    gasPrice = parseUnits('3.3', 'gwei');
                }

                // If the config overrides default gas limit we override
                const defaultGasLimit = instanceGasLimit ? BigInt(instanceGasLimit) : BigInt(DEFAULT_GAS_LIMIT);
                let gasLimit = defaultGasLimit;

                // If the denomination is small only refund small amount otherwise use the default value
                const refundGasLimit = isFirstAmount && tokenGasLimit ? BigInt(tokenGasLimit) : undefined;

                async function getProof() {
                    let relayer = ZeroAddress;
                    let fee = BigInt(0);
                    let refund = BigInt(0);

                    if (!walletWithdrawal) {
                        if (!isEth) {
                            refund = ethPurchase
                                ? parseEther(`${ethPurchase}`)
                                : tornadoFeeOracle.defaultEthRefund(gasPrice, refundGasLimit);
                        }

                        const { rewardAccount, tornadoServiceFee: relayerFeePercent } =
                            relayerClient?.selectedRelayer || {};

                        fee = tornadoFeeOracle.calculateRelayerFee({
                            gasPrice,
                            gasLimit,
                            l1Fee,
                            denomination,
                            ethRefund: refund,
                            tokenPriceInWei,
                            tokenDecimals: decimals,
                            relayerFeePercent,
                            isEth,
                        });

                        relayer = rewardAccount as string;

                        if (fee > denomination) {
                            const errMsg =
                                `Relayer fee ${formatUnits(fee, decimals)} ${currency.toUpperCase()} ` +
                                `exceeds the deposit amount ${amount} ${currency.toUpperCase()}.`;
                            throw new Error(errMsg);
                        }
                    }

                    const { proof, args } = await calculateSnarkProof(
                        {
                            root: tree.root,
                            nullifierHex,
                            recipient,
                            relayer,
                            fee,
                            refund,
                            nullifier,
                            secret,
                            pathElements,
                            pathIndices,
                        },
                        circuit,
                        provingKey,
                    );

                    return {
                        fee,
                        refund,
                        proof,
                        args,
                    };
                }

                let { fee, refund, proof, args } = await getProof();

                const withdrawOverrides = {
                    // from: !walletWithdrawal ? relayerClient?.selectedRelayer?.rewardAccount : (signer?.address as string),
                    value: args[5] || 0,
                };

                gasLimit = await TornadoProxy.withdraw.estimateGas(instanceAddress, proof, ...args, withdrawOverrides);

                if (fee) {
                    ({ fee, refund, proof, args } = await getProof());

                    // Verify if our recalculated proof can be withdrawn
                    await TornadoProxy.withdraw.estimateGas(instanceAddress, proof, ...args, withdrawOverrides);
                }

                const txFee = gasPrice * gasLimit;
                const txFeeInToken = !isEth
                    ? tornadoFeeOracle.calculateTokenAmount(txFee, tokenPriceInWei, decimals)
                    : BigInt(0);
                const txFeeString = !isEth
                    ? `( ${Number(formatUnits(txFeeInToken, decimals)).toFixed(5)} ${currency.toUpperCase()} worth ) ` +
                      `( ${((Number(formatUnits(txFeeInToken, decimals)) / Number(amount)) * 100).toFixed(5)}% )`
                    : `( ${((Number(formatUnits(txFee, decimals)) / Number(amount)) * 100).toFixed(5)}% )`;

                const withdrawTable = new Table();
                withdrawTable.push(
                    [
                        {
                            colSpan: 2,
                            content: 'Withdrawal Info',
                            hAlign: 'center',
                        },
                    ],
                    [
                        'Deposit Date',
                        `${depositDate.toLocaleDateString()} ${depositDate.toLocaleTimeString()} (${moment.unix(depositEvent.timestamp).fromNow()})`,
                    ],
                    ['From', depositEvent.from],
                    ['Deposit Transaction', depositEvent.transactionHash],
                    ['Commitment', depositEvent.commitment],
                    ['Gas Price', `${formatUnits(gasPrice, 'gwei')} gwei`],
                    ['Gas Limit', gasLimit],
                    [
                        'Transaction Fee',
                        `${Number(formatEther(txFee)).toFixed(5)} ${nativeCurrency.toUpperCase()} ${txFeeString}`,
                    ],
                );

                // withdraw using relayer
                if (!walletWithdrawal) {
                    const relayerFeePercent = Number(relayerClient?.selectedRelayer?.tornadoServiceFee) || 0;
                    const relayerFee =
                        (BigInt(denomination) * BigInt(Math.floor(10000 * relayerFeePercent))) / BigInt(10000 * 100);

                    withdrawTable.push(
                        ['Relayer', `${relayerClient?.selectedRelayer?.url}`],
                        [
                            'Relayer Fee',
                            `${formatUnits(relayerFee, decimals)} ${currency.toUpperCase()} ` +
                                `( ${relayerFeePercent}% )`,
                        ],
                        [
                            'Total Fee',
                            `${formatUnits(fee, decimals)} ${currency.toUpperCase()} ` +
                                `( ${((Number(fee) / Number(denomination)) * 100).toFixed(5)}% )`,
                        ],
                        [
                            'Amount to receive',
                            `${Number(formatUnits(denomination - fee, decimals)).toFixed(5)} ${currency.toUpperCase()}`,
                        ],
                        [
                            `${nativeCurrency.toUpperCase()} purchase`,
                            `${formatEther(refund)} ${nativeCurrency.toUpperCase()}`,
                        ],
                        ['To', recipient],
                        ['Nullifier', nullifierHex],
                    );

                    console.log('\n' + withdrawTable.toString() + '\n');

                    await promptConfirmation(options.nonInteractive);

                    console.log('Sending withdraw transaction through relay\n');

                    await relayerClient.tornadoWithdraw({
                        contract: instanceAddress,
                        proof,
                        args,
                    });
                } else {
                    // withdraw from wallet
                    withdrawTable.push(
                        ['Signer', `${signer?.address}`],
                        ['Amount to receive', `${amount} ${currency.toUpperCase()}`],
                        ['To', recipient],
                        ['Nullifier', nullifierHex],
                    );

                    console.log('\n' + withdrawTable.toString() + '\n');

                    await promptConfirmation(options.nonInteractive);

                    console.log('Sending withdraw transaction through wallet\n');

                    await programSendTransaction({
                        signer: signer as TornadoVoidSigner | TornadoWallet,
                        options,
                        populatedTransaction: await TornadoProxy.withdraw.populateTransaction(
                            instanceAddress,
                            proof,
                            ...args,
                        ),
                    });
                }

                process.exit(0);
            },
        );

    program
        .command('compliance')
        .description(
            'Shows the deposit and withdrawal of the provided note. \n\n' +
                'This might be necessary to show the origin of assets held in your withdrawal address. \n\n',
        )
        .argument('<note>', 'Tornado Cash Deposit Note')
        .action(async (note: string, cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
            const { rpc, disableTovarish } = options;

            const deposit = await Deposit.parseNote(note);
            const { netId, currency, amount, commitmentHex, nullifierHex } = deposit;

            const config = getConfig(netId);

            const {
                deployedBlock,
                nativeCurrency,
                tokens: { [currency]: currencyConfig },
            } = config;

            const {
                decimals,
                instanceAddress: { [amount]: instanceAddress },
            } = currencyConfig;

            const provider = await getProgramProvider(rpc, {
                netId,
                ...fetchDataOptions,
            });

            const tovarishClient = !disableTovarish
                ? (
                      await getTovarishRelayer({
                          options,
                          fetchDataOptions,
                          netId,
                      })
                  ).relayerClient
                : undefined;

            if (tovarishClient?.selectedRelayer) {
                console.log(`\nConnected with Tovarish Relayer ${tovarishClient.selectedRelayer.url}\n`);
            }

            const Tornado = Tornado__factory.connect(instanceAddress, provider);

            const TornadoServiceConstructor = {
                netId,
                provider,
                Tornado,
                amount,
                currency,
                deployedBlock,
                fetchDataOptions,
                tovarishClient,
                cacheDirectory: EVENTS_DIR,
                userDirectory: SAVED_DIR,
                nativeCurrency,
            };

            const depositsService = new NodeTornadoService({
                ...TornadoServiceConstructor,
                type: 'Deposit',
                merkleTreeService: new MerkleTreeService({
                    netId,
                    amount,
                    currency,
                    Tornado,
                    merkleWorkerPath,
                }),
                optionalTree: true,
            });

            const withdrawalsService = new NodeTornadoService({
                ...TornadoServiceConstructor,
                type: 'Withdrawal',
            });

            const depositEvents = (await depositsService.updateEvents()).events as DepositsEvents[];

            const withdrawalEvents = (await withdrawalsService.updateEvents()).events as WithdrawalsEvents[];

            const depositEvent = depositEvents.find(({ commitment }) => commitment === commitmentHex);

            const withdrawalEvent = withdrawalEvents.find(({ nullifierHash }) => nullifierHash === nullifierHex);

            const complianceTable = new Table();
            complianceTable.push([{ colSpan: 2, content: 'Compliance Info', hAlign: 'center' }]);

            if (!depositEvent) {
                complianceTable.push([{ colSpan: 2, content: 'Deposit', hAlign: 'center' }], ['Deposit', 'Not Found']);
            } else {
                const depositDate = new Date(depositEvent.timestamp * 1000);

                complianceTable.push(
                    [{ colSpan: 2, content: 'Deposit', hAlign: 'center' }],
                    ['Deposit', `${amount} ${currency.toUpperCase()}`],
                    [
                        'Date',
                        `${depositDate.toLocaleDateString()} ${depositDate.toLocaleTimeString()} (${moment.unix(depositEvent.timestamp).fromNow()})`,
                    ],
                    ['From', depositEvent.from],
                    ['Transaction', depositEvent.transactionHash],
                    ['Commitment', commitmentHex],
                    ['Spent', Boolean(withdrawalEvent)],
                );
            }

            if (withdrawalEvent) {
                const withdrawalDate = new Date(withdrawalEvent.timestamp * 1000);

                complianceTable.push(
                    [{ colSpan: 2, content: 'Withdraw', hAlign: 'center' }],
                    ['Withdrawal', `${amount} ${currency.toUpperCase()}`],
                    ['Relayer Fee', `${formatUnits(withdrawalEvent.fee, decimals)} ${currency.toUpperCase()}`],
                    [
                        'Date',
                        `${withdrawalDate.toLocaleDateString()} ${withdrawalDate.toLocaleTimeString()} (${moment.unix(withdrawalEvent.timestamp).fromNow()})`,
                    ],
                    ['To', withdrawalEvent.to],
                    ['Transaction', withdrawalEvent.transactionHash],
                    ['Nullifier', nullifierHex],
                );
            }

            console.log('\n\n' + complianceTable.toString() + '\n');

            process.exit(0);
        });

    program
        .command('updateEvents')
        .description('Sync the local cache file of tornado cash events.\n\n')
        .argument('[netId]', 'Network Chain ID to connect with (see https://chainlist.org for examples)', parseNumber)
        .argument('[currency]', 'Currency to sync events')
        .action(
            async (
                netIdOpts: NetIdType | undefined,
                currencyOpts: string | undefined,
                cmdOptions: commonProgramOptions,
            ) => {
                const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
                const { rpc, disableTovarish } = options;

                const networks = netIdOpts ? [netIdOpts] : enabledChains;

                for (const netId of networks) {
                    const config = getConfig(netId);
                    const {
                        tokens,
                        nativeCurrency,
                        routerContract,
                        echoContract,
                        registryContract,
                        reverseRecordsContract,
                        aggregatorContract,
                        governanceContract,
                        deployedBlock,
                        constants: { GOVERNANCE_BLOCK, REGISTRY_BLOCK, NOTE_ACCOUNT_BLOCK, ENCRYPTED_NOTES_BLOCK },
                    } = config;

                    const provider = await getProgramProvider(rpc, {
                        netId,
                        ...fetchDataOptions,
                    });

                    const tovarishClient = !disableTovarish
                        ? (
                              await getTovarishRelayer({
                                  options,
                                  fetchDataOptions,
                                  netId,
                              })
                          ).relayerClient
                        : undefined;

                    if (tovarishClient?.selectedRelayer) {
                        console.log(`\nConnected with Tovarish Relayer ${tovarishClient.selectedRelayer.url}\n`);
                    }

                    if (
                        netId === RELAYER_NETWORK &&
                        governanceContract &&
                        aggregatorContract &&
                        reverseRecordsContract
                    ) {
                        const governanceService = new NodeGovernanceService({
                            netId,
                            provider,
                            Governance: Governance__factory.connect(governanceContract, provider),
                            Aggregator: Aggregator__factory.connect(aggregatorContract, provider),
                            ReverseRecords: ReverseRecords__factory.connect(reverseRecordsContract, provider),
                            deployedBlock: GOVERNANCE_BLOCK,
                            fetchDataOptions,
                            tovarishClient,
                            cacheDirectory: EVENTS_DIR,
                            userDirectory: SAVED_DIR,
                        });

                        await governanceService.updateEvents();
                    }

                    if (netId === RELAYER_NETWORK && registryContract && aggregatorContract) {
                        const registryService = new NodeRegistryService({
                            netId,
                            provider,
                            RelayerRegistry: RelayerRegistry__factory.connect(registryContract, provider),
                            Aggregator: Aggregator__factory.connect(aggregatorContract, provider),
                            relayerEnsSubdomains: getRelayerEnsSubdomains(),
                            deployedBlock: REGISTRY_BLOCK,
                            fetchDataOptions,
                            tovarishClient,
                            cacheDirectory: EVENTS_DIR,
                            userDirectory: SAVED_DIR,
                        });

                        await registryService.updateEvents();

                        await registryService.updateRelayers();

                        const revenueService = new NodeRevenueService({
                            netId,
                            provider,
                            RelayerRegistry: RelayerRegistry__factory.connect(registryContract, provider),
                            deployedBlock: REGISTRY_BLOCK,
                            fetchDataOptions,
                            tovarishClient,
                            cacheDirectory: EVENTS_DIR,
                            userDirectory: SAVED_DIR,
                        });

                        await revenueService.updateEvents();
                    }

                    const echoService = new NodeEchoService({
                        netId,
                        provider,
                        Echoer: Echoer__factory.connect(echoContract, provider),
                        deployedBlock: NOTE_ACCOUNT_BLOCK,
                        fetchDataOptions,
                        tovarishClient,
                        cacheDirectory: EVENTS_DIR,
                        userDirectory: SAVED_DIR,
                    });

                    await echoService.updateEvents();

                    const encryptedNotesService = new NodeEncryptedNotesService({
                        netId,
                        provider,
                        Router: TornadoRouter__factory.connect(routerContract, provider),
                        deployedBlock: ENCRYPTED_NOTES_BLOCK,
                        fetchDataOptions,
                        tovarishClient,
                        cacheDirectory: EVENTS_DIR,
                        userDirectory: SAVED_DIR,
                    });

                    await encryptedNotesService.updateEvents();

                    const currencies = currencyOpts ? [currencyOpts.toLowerCase()] : getActiveTokens(config);

                    for (const currency of currencies) {
                        const currencyConfig = tokens[currency];
                        // Now load the denominations and address
                        const instance = Object.entries(currencyConfig.instanceAddress);

                        // And now sync
                        for (const [amount, instanceAddress] of instance) {
                            const Tornado = Tornado__factory.connect(instanceAddress, provider);

                            const TornadoServiceConstructor = {
                                netId,
                                provider,
                                Tornado,
                                amount,
                                currency,
                                deployedBlock,
                                fetchDataOptions,
                                tovarishClient,
                                cacheDirectory: EVENTS_DIR,
                                userDirectory: SAVED_DIR,
                                nativeCurrency,
                            };

                            const depositsService = new NodeTornadoService({
                                ...TornadoServiceConstructor,
                                type: 'Deposit',
                                merkleTreeService: new MerkleTreeService({
                                    netId,
                                    amount,
                                    currency,
                                    Tornado,
                                    merkleWorkerPath,
                                }),
                                treeCache: new TreeCache({
                                    netId,
                                    amount,
                                    currency,
                                    userDirectory: SAVED_TREE_DIR,
                                }),
                                optionalTree: true,
                            });

                            const withdrawalsService = new NodeTornadoService({
                                ...TornadoServiceConstructor,
                                type: 'Withdrawal',
                            });

                            await depositsService.updateEvents();

                            await withdrawalsService.updateEvents();
                        }
                    }
                }

                process.exit(0);
            },
        );

    program
        .command('relayers')
        .description('List all registered relayers from the tornado cash registry.\n\n')
        .argument('<netId>', 'Network Chain ID to connect with (see https://chainlist.org for examples)', parseNumber)
        .action(async (netIdOpts: NetIdType, cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);

            const allRelayers = options.disableTovarish
                ? await getProgramRelayer({
                      options,
                      fetchDataOptions,
                      netId: netIdOpts,
                  })
                : await getTovarishRelayer({
                      options,
                      fetchDataOptions,
                      netId: netIdOpts,
                  });

            const validRelayers = allRelayers.validRelayers as RelayerInfo[];
            const invalidRelayers = allRelayers.invalidRelayers as RelayerError[];

            const relayersTable = new Table();
            const relayerName = options.disableTovarish ? 'Relayers' : 'Tovarish Relayers';

            relayersTable.push(
                [{ colSpan: 8, content: relayerName, hAlign: 'center' }],
                [
                    'netId',
                    'url',
                    'ensName',
                    'stakeBalance',
                    'relayerAddress',
                    'rewardAccount',
                    'currentQueue',
                    'serviceFee',
                ].map((content) => ({ content: colors.red.bold(content) })),
                ...validRelayers.map(
                    ({
                        netId,
                        url,
                        ensName,
                        stakeBalance,
                        relayerAddress,
                        rewardAccount,
                        currentQueue,
                        tornadoServiceFee,
                    }) => {
                        return [
                            netId,
                            url,
                            ensName,
                            stakeBalance ? `${Number(stakeBalance).toFixed(5)} TORN` : '',
                            relayerAddress,
                            rewardAccount,
                            currentQueue,
                            `${tornadoServiceFee}%`,
                        ];
                    },
                ),
            );

            const invalidRelayersTable = new Table();

            invalidRelayersTable.push(
                [
                    {
                        colSpan: 3,
                        content: `Invalid ${relayerName}`,
                        hAlign: 'center',
                    },
                ],
                ['hostname', 'relayerAddress', 'errorMessage'].map((content) => ({
                    content: colors.red.bold(content),
                })),
                ...invalidRelayers.map(({ hostname, relayerAddress, errorMessage }) => {
                    return [hostname, relayerAddress, errorMessage ? substring(errorMessage, 40) : ''];
                }),
            );

            console.log(relayersTable.toString() + '\n');
            console.log(invalidRelayersTable.toString() + '\n');

            process.exit(0);
        });

    program
        .command('createAccount')
        .description(
            'Creates and save on-chain account that would store encrypted notes. \n\n' +
                'Would first lookup on on-chain records to see if the notes are stored. \n\n' +
                'Requires a valid signable wallet (mnemonic or a private key) to work (Since they would encrypt or encrypted)',
        )
        .argument('<netId>', 'Network Chain ID to connect with (see https://chainlist.org for examples)', parseNumber)
        .action(async (netId: NetIdType, cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
            const { rpc, disableTovarish } = options;

            const config = getConfig(netId);

            const {
                echoContract,
                constants: { NOTE_ACCOUNT_BLOCK },
            } = config;

            const provider = await getProgramProvider(rpc, {
                netId,
                ...fetchDataOptions,
            });

            const signer = getProgramSigner({
                options,
                provider,
            });

            if (!signer || signer instanceof VoidSigner) {
                throw new Error(
                    'No wallet found, make your you have supplied a valid mnemonic or private key before using this command',
                );
            }

            const tovarishClient = !disableTovarish
                ? (
                      await getTovarishRelayer({
                          options,
                          fetchDataOptions,
                          netId,
                      })
                  ).relayerClient
                : undefined;

            if (tovarishClient?.selectedRelayer) {
                console.log(`\nConnected with Tovarish Relayer ${tovarishClient.selectedRelayer.url}\n`);
            }

            /**
             * Find for any existing note accounts
             */
            const Echoer = Echoer__factory.connect(echoContract, provider);

            const echoService = new NodeEchoService({
                netId,
                provider,
                Echoer,
                deployedBlock: NOTE_ACCOUNT_BLOCK,
                fetchDataOptions,
                tovarishClient,
                cacheDirectory: EVENTS_DIR,
                userDirectory: SAVED_DIR,
            });

            console.log('Getting historic note accounts would take a while\n');

            const echoEvents = (await echoService.updateEvents()).events;

            const existingAccounts = await NoteAccount.decryptSignerNoteAccounts(signer, echoEvents);

            const accountsTable = new Table();

            if (existingAccounts.length) {
                accountsTable.push(
                    [
                        {
                            colSpan: 2,
                            content: `Note Accounts (${netId})`,
                            hAlign: 'center',
                        },
                    ],
                    [
                        {
                            colSpan: 2,
                            content: `Backed up by: ${signer.address}`,
                            hAlign: 'center',
                        },
                    ],
                    ['blockNumber', 'noteAccount'].map((content) => ({
                        content: colors.red.bold(content),
                    })),
                    ...existingAccounts.map(({ blockNumber, recoveryKey }) => {
                        return [blockNumber, recoveryKey];
                    }),
                );

                console.log(accountsTable.toString() + '\n');
            } else {
                const newAccount = new NoteAccount({});

                accountsTable.push(
                    [
                        {
                            colSpan: 1,
                            content: `New Note Account (${netId})`,
                            hAlign: 'center',
                        },
                    ],
                    ['noteAccount'].map((content) => ({
                        content: colors.red.bold(content),
                    })),
                    [newAccount.recoveryKey],
                    [
                        {
                            colSpan: 1,
                            content: `Would be backed up by: ${signer.address}`,
                            hAlign: 'center',
                        },
                    ],
                );

                const fileName = `backup-note-account-key-0x${newAccount.recoveryKey.slice(0, 8)}.txt`;

                console.log('\n' + accountsTable.toString() + '\n');

                console.log(`Writing backup to ${fileName}\n`);

                await writeFile(fileName, newAccount.recoveryKey + '\n');

                console.log('Backup encrypted account on-chain to use on UI?\n');

                await promptConfirmation(options.nonInteractive);

                const signerPublicKey = await NoteAccount.getSignerPublicKey(signer);

                const { data } = newAccount.getEncryptedAccount(signerPublicKey);

                console.log('Sending encrypted note account backup transaction through wallet\n');

                await programSendTransaction({
                    signer: signer as TornadoVoidSigner | TornadoWallet,
                    options,
                    populatedTransaction: await Echoer.echo.populateTransaction(data),
                });
            }

            process.exit(0);
        });

    program
        .command('decrypt')
        .description('Fetch encryption keys and encrypted notes from on-chain. \n\n')
        .argument('<netId>', 'Network Chain ID to connect with (see https://chainlist.org for examples)', parseNumber)
        .argument(
            '[accountKey]',
            'Account key generated from UI or the createAccount to store encrypted notes on-chain',
            parseRecoveryKey,
        )
        .action(async (netId: NetIdType, accountKey: string | undefined, cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
            const { rpc, disableTovarish } = options;
            if (!accountKey) {
                accountKey = options.accountKey;
            }

            const config = getConfig(netId);

            const {
                routerContract,
                echoContract,
                constants: { NOTE_ACCOUNT_BLOCK, ENCRYPTED_NOTES_BLOCK },
            } = config;

            const provider = await getProgramProvider(rpc, {
                netId,
                ...fetchDataOptions,
            });

            const tovarishClient = !disableTovarish
                ? (
                      await getTovarishRelayer({
                          options,
                          fetchDataOptions,
                          netId,
                      })
                  ).relayerClient
                : undefined;

            if (tovarishClient?.selectedRelayer) {
                console.log(`\nConnected with Tovarish Relayer ${tovarishClient.selectedRelayer.url}\n`);
            }

            const Echoer = Echoer__factory.connect(echoContract, provider);

            const echoService = new NodeEchoService({
                netId,
                provider,
                Echoer,
                deployedBlock: NOTE_ACCOUNT_BLOCK,
                fetchDataOptions,
                tovarishClient,
                cacheDirectory: EVENTS_DIR,
                userDirectory: SAVED_DIR,
            });

            const encryptedNotesService = new NodeEncryptedNotesService({
                netId,
                provider,
                Router: TornadoRouter__factory.connect(routerContract, provider),
                deployedBlock: ENCRYPTED_NOTES_BLOCK,
                fetchDataOptions,
                tovarishClient,
                cacheDirectory: EVENTS_DIR,
                userDirectory: SAVED_DIR,
            });

            const accounts = [];

            // Recover encryption keys possibly encrypted by a signer
            const signer = getProgramSigner({
                options,
                provider,
            }) as TornadoWallet;

            if (signer?.privateKey) {
                const echoEvents = (await echoService.updateEvents()).events;

                accounts.push(...(await NoteAccount.decryptSignerNoteAccounts(signer, echoEvents)));
            }

            if (accountKey && !accounts.find(({ recoveryKey }) => recoveryKey === accountKey)) {
                accounts.push(new NoteAccount({ recoveryKey: accountKey }));
            }

            if (!accounts.length) {
                throw new Error(
                    'No encryption key find! Please supply encryption key from either UI or create one with createAccount command',
                );
            }

            const accountsTable = new Table();

            accountsTable.push(
                [
                    {
                        colSpan: 2,
                        content: `Note Accounts (${netId})`,
                        hAlign: 'center',
                    },
                ],
                [
                    {
                        colSpan: 2,
                        content: `Backed up by: ${signer?.address}`,
                        hAlign: 'center',
                    },
                ],
                ['blockNumber', 'noteAccount'].map((content) => ({
                    content: colors.red.bold(content),
                })),
                ...accounts.map(({ blockNumber, recoveryKey }) => {
                    return [blockNumber, recoveryKey];
                }),
            );

            // Decrypting notes
            const encryptedNoteEvents = (await encryptedNotesService.updateEvents()).events;

            const decryptedNotes = accounts
                .map((noteAccount) => noteAccount.decryptNotes(encryptedNoteEvents))
                .flat()
                .map(({ blockNumber, address, noteHex }) => {
                    const { amount, currency } = getInstanceByAddress(config, address) || {};

                    if (amount) {
                        return [blockNumber, `tornado-${currency}-${amount}-${netId}-${noteHex}`];
                    }

                    return [blockNumber, noteHex];
                });

            const notesTable = new Table();

            notesTable.push(
                [
                    {
                        colSpan: 2,
                        content: `Note Accounts (${netId})`,
                        hAlign: 'center',
                    },
                ],
                [
                    {
                        colSpan: 2,
                        content: `Account key: ${accountKey}`,
                        hAlign: 'center',
                    },
                ],
                ['blockNumber', 'note'].map((content) => ({
                    content: colors.red.bold(content),
                })),
                ...decryptedNotes,
            );

            console.log(accountsTable.toString() + '\n');

            console.log('\n' + notesTable.toString() + '\n');

            process.exit(0);
        });

    program
        .command('send')
        .description('Send ETH or ERC20 token to address.\n\n')
        .argument('<netId>', 'Network Chain ID to connect with (see https://chainlist.org for examples)', parseNumber)
        .argument('<to>', 'To address', parseAddress)
        .argument('[amount]', 'Sending amounts', parseNumber)
        .argument('[token]', 'ERC20 Token Contract to check Token Balance', parseAddress)
        .action(
            async (
                netId: NetIdType,
                to: string,
                amountArgs: number | undefined,
                tokenArgs: string | undefined,
                cmdOptions: commonProgramOptions,
            ) => {
                const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
                const { rpc, token: tokenOpts } = options;

                const config = getConfig(netId);

                const { currencyName, multicallContract } = config;

                const provider = await getProgramProvider(rpc, {
                    netId,
                    ...fetchDataOptions,
                });

                const signer = getProgramSigner({ options, provider });

                if (!signer) {
                    throw new Error(
                        'Signer not defined, make sure you have either viewOnly address, mnemonic, or private key configured',
                    );
                }

                const tokenAddress = tokenArgs ? parseAddress(tokenArgs) : tokenOpts;

                const Multicall = Multicall__factory.connect(multicallContract, provider);
                const Token = (tokenAddress ? ERC20__factory.connect(tokenAddress, signer) : undefined) as ERC20;

                // Fetching feeData or nonce is unnecessary however we do this to estimate transfer amounts
                const [feeData, nonce, [{ balance: ethBalance }, tokenResults]] = await Promise.all([
                    provider.getFeeData(),
                    provider.getTransactionCount(signer.address, 'pending'),
                    getTokenBalances({
                        provider,
                        Multicall,
                        currencyName,
                        userAddress: signer.address,
                        tokenAddresses: tokenAddress ? [tokenAddress] : [],
                    }),
                ]);

                const {
                    symbol: tokenSymbol,
                    decimals: tokenDecimals,
                    balance: tokenBalance,
                }: tokenBalances = tokenResults || {};

                const txType = feeData.maxFeePerGas ? 2 : 0;
                const txGasPrice = feeData.maxFeePerGas
                    ? feeData.maxFeePerGas + (feeData.maxPriorityFeePerGas || BigInt(0))
                    : feeData.gasPrice || BigInt(0);
                const txFees = feeData.maxFeePerGas
                    ? {
                          maxFeePerGas: feeData.maxFeePerGas,
                          maxPriorityFeePerGas: feeData.maxPriorityFeePerGas,
                      }
                    : {
                          gasPrice: feeData.gasPrice,
                      };

                let toSend: bigint;

                if (amountArgs) {
                    if (tokenAddress) {
                        toSend = parseUnits(`${amountArgs}`, tokenDecimals);

                        if (toSend > tokenBalance) {
                            const errMsg = `Invalid ${tokenSymbol} balance, wants ${amountArgs} have ${formatUnits(tokenBalance, tokenDecimals)}`;
                            throw new Error(errMsg);
                        }
                    } else {
                        toSend = parseEther(`${amountArgs}`);

                        if (toSend > ethBalance) {
                            const errMsg = `Invalid ${currencyName} balance, wants ${amountArgs} have ${formatEther(ethBalance)}`;
                            throw new Error(errMsg);
                        }
                    }
                } else {
                    if (tokenAddress) {
                        toSend = tokenBalance;
                    } else {
                        const initCost = txGasPrice * BigInt('400000');
                        toSend = ethBalance - initCost;

                        if (ethBalance === BigInt(0) || ethBalance < initCost) {
                            const errMsg = `Invalid ${currencyName} balance, wants ${formatEther(initCost)} have ${formatEther(ethBalance)}`;
                            throw new Error(errMsg);
                        }

                        const estimatedGas = await provider.estimateGas({
                            type: txType,
                            from: signer.address,
                            to,
                            value: toSend,
                            nonce,
                            ...txFees,
                        });

                        const bumpedGas =
                            estimatedGas !== BigInt(21000) && signer.gasLimitBump
                                ? (estimatedGas * (BigInt(10000) + BigInt(signer.gasLimitBump))) / BigInt(10000)
                                : estimatedGas;

                        toSend = ethBalance - txGasPrice * bumpedGas;
                    }
                }

                await programSendTransaction({
                    signer,
                    options,
                    populatedTransaction: tokenAddress
                        ? await Token.transfer.populateTransaction(to, toSend)
                        : await signer.populateTransaction({
                              type: txType,
                              from: signer.address,
                              to,
                              value: toSend,
                              nonce,
                              ...txFees,
                          }),
                });

                process.exit(0);
            },
        );

    program
        .command('balance')
        .description('Check ETH and ERC20 balance.\n\n')
        .argument('<netId>', 'Network Chain ID to connect with (see https://chainlist.org for examples)', parseNumber)
        .argument('[address]', 'ETH Address to check balance', parseAddress)
        .argument('[token]', 'ERC20 Token Contract to check Token Balance', parseAddress)
        .action(
            async (
                netId: NetIdType,
                addressArgs: string | undefined,
                tokenArgs: string | undefined,
                cmdOptions: commonProgramOptions,
            ) => {
                const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
                const { rpc, token: tokenOpts } = options;

                const config = getConfig(netId);

                const { currencyName, multicallContract, tornContract, tokens } = config;

                const provider = await getProgramProvider(rpc, {
                    netId,
                    ...fetchDataOptions,
                });

                const userAddress = addressArgs
                    ? parseAddress(addressArgs)
                    : getProgramSigner({ options, provider })?.address;
                const tokenAddress = tokenArgs ? parseAddress(tokenArgs) : tokenOpts;

                if (!userAddress) {
                    throw new Error('Address is required however no user address is supplied');
                }

                const Multicall = Multicall__factory.connect(multicallContract, provider);

                const tokenAddresses = Object.values(tokens)
                    .map(({ tokenAddress }) => tokenAddress)
                    .filter((t) => t) as string[];

                if (tornContract) {
                    tokenAddresses.push(tornContract);
                }

                const tokenBalances = await getTokenBalances({
                    provider,
                    Multicall,
                    currencyName,
                    userAddress,
                    tokenAddresses: [...(tokenAddress ? [tokenAddress] : tokenAddresses)],
                });

                const balanceTable = new Table({
                    head: ['Token', 'Contract Address', 'Balance'],
                });

                balanceTable.push(
                    [
                        {
                            colSpan: 3,
                            content: `User: ${userAddress}`,
                            hAlign: 'center',
                        },
                    ],
                    ...tokenBalances.map(({ address, name, symbol, decimals, balance }) => {
                        return [`${name} (${symbol})`, address, `${formatUnits(balance, decimals)} ${symbol}`];
                    }),
                );

                console.log(balanceTable.toString());

                process.exit(0);
            },
        );

    program
        .command('sign')
        .description('Sign unsigned transaction with signer.\n\n')
        .argument('<unsignedTx>', 'Unsigned Transaction')
        .action(async (unsignedTx: string, cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
            const { rpc } = options;

            const deserializedTx = Transaction.from(unsignedTx).toJSON();

            const netId = Number(deserializedTx.chainId);

            const provider = await getProgramProvider(rpc, {
                netId,
                ...fetchDataOptions,
            });

            const signer = getProgramSigner({ options, provider });

            if (!signer || signer instanceof VoidSigner) {
                throw new Error('Signer not defined or not signable signer');
            }

            await programSendTransaction({
                signer,
                options,
                populatedTransaction: deserializedTx,
            });

            process.exit(0);
        });

    program
        .command('broadcast')
        .description('Broadcast signed transaction.\n\n')
        .argument('<signedTx>', 'Signed Transaction')
        .action(async (signedTx: string, cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
            const { rpc } = options;

            const netId = Number(Transaction.from(signedTx).chainId);

            if (!netId) {
                throw new Error(
                    'NetId for the transaction is invalid, this command only supports EIP-155 transactions',
                );
            }

            const provider = await getProgramProvider(rpc, {
                netId,
                ...fetchDataOptions,
            });

            const { hash } = await provider.broadcastTransaction(signedTx);

            console.log(`\nBroadcastd tx: ${hash}\n`);

            process.exit(0);
        });

    program
        .command('proposals')
        .description('Get list or detail about TORN proposal')
        .argument('[proposalId]', 'Proposal ID')
        .action(async (proposalId: number, cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
            const { rpc, disableTovarish } = options;
            const netId = RELAYER_NETWORK;

            const provider = await getProgramProvider(rpc, {
                netId,
                ...fetchDataOptions,
            });

            const config = getConfig(netId);

            const {
                governanceContract,
                aggregatorContract,
                reverseRecordsContract,
                constants: { GOVERNANCE_BLOCK },
            } = config;

            const tovarishClient = !disableTovarish
                ? (
                      await getTovarishRelayer({
                          options,
                          fetchDataOptions,
                          netId,
                      })
                  ).relayerClient
                : undefined;

            if (tovarishClient?.selectedRelayer) {
                console.log(`\nConnected with Tovarish Relayer ${tovarishClient.selectedRelayer.url}\n`);
            }

            const governanceService = new NodeGovernanceService({
                netId,
                provider,
                Governance: Governance__factory.connect(governanceContract as string, provider),
                Aggregator: Aggregator__factory.connect(aggregatorContract as string, provider),
                ReverseRecords: ReverseRecords__factory.connect(reverseRecordsContract as string, provider),
                deployedBlock: GOVERNANCE_BLOCK,
                fetchDataOptions,
                tovarishClient,
                cacheDirectory: EVENTS_DIR,
                userDirectory: SAVED_DIR,
            });

            const proposals = await governanceService.getAllProposals();

            const proposal = proposals.find((p) => p.id === Number(proposalId));

            if (!proposal) {
                const recentProposals = proposals.reverse().slice(0, 20);

                const proposalTable = new Table();

                proposalTable.push(
                    [
                        {
                            colSpan: 9,
                            content: 'Last 20 Proposals',
                            hAlign: 'center',
                        },
                    ],
                    [
                        'ID',
                        'Title',
                        'Proposer',
                        'Start Time',
                        'End Time',
                        'Quorum',
                        'For Votes',
                        'Against Votes',
                        'State',
                    ],
                    ...recentProposals.map(
                        ({
                            id,
                            title,
                            proposer,
                            proposerName,
                            startTime,
                            endTime,
                            quorum,
                            forVotes,
                            againstVotes,
                            state,
                        }) => {
                            return [
                                id,
                                title,
                                proposerName || proposer.slice(0, 20),
                                moment.unix(startTime).format('DD/MM/YYYY'),
                                moment.unix(endTime).format('DD/MM/YYYY'),
                                quorum,
                                numberFormatter(formatEther(forVotes)) + ' TORN',
                                numberFormatter(formatEther(againstVotes)) + ' TORN',
                                state,
                            ];
                        },
                    ),
                );

                console.log(proposalTable.toString());

                process.exit(0);
            }

            proposalId = Number(proposalId);

            const {
                transactionHash,
                proposer,
                proposerName,
                target,
                startTime,
                endTime,
                description,
                title,
                forVotes,
                againstVotes,
                executed,
                extended,
                quorum,
                state,
            } = proposal;

            const allVotes = forVotes + againstVotes;

            const proposalTable = new Table();

            proposalTable.push(
                [
                    {
                        colSpan: 2,
                        content: `Proposal ${proposalId}`,
                        hAlign: 'center',
                    },
                ],
                ['Title', title],
                ['Description', description],
                ['Proposer', proposerName || proposer],
                ['Proposal Address', target],
                ['Proposal TX', transactionHash],
                ['Start Time', String(moment.unix(startTime).toDate())],
                ['End Time', String(moment.unix(endTime).toDate())],
                ['For', numberFormatter(formatEther(forVotes)) + ' TORN'],
                ['Against', numberFormatter(formatEther(againstVotes)) + ' TORN'],
                ['Quorum', quorum],
                ['State', state],
                ['Extended', extended],
                ['Executed', executed],
            );

            console.log(proposalTable.toString());

            const votes = await governanceService.getVotes(proposalId);

            const votersSet = new Set();
            const uniqueVotes = votes
                .reverse()
                .filter((v) => {
                    if (!votersSet.has(v.voter)) {
                        votersSet.add(v.voter);
                        return true;
                    }
                    return false;
                })
                .sort((a, b) => Number(b.votes) - Number(a.votes));

            const commentTable = new Table();

            commentTable.push(
                [{ colSpan: 5, content: 'Votes', hAlign: 'center' }],
                ['Support', 'Votes', 'Voter', 'Delegate', 'Percent'],
                ...uniqueVotes.map(({ support, votes, voter, voterName, from, fromName }) => {
                    const supportStr = support ? 'For' : 'Against';
                    const votesStr = numberFormatter(formatEther(votes)) + ' TORN';
                    const delegate = voter !== from ? fromName || from.slice(0, 20) : '';
                    const percentage = ((Number(votes) / Number(allVotes)) * 100).toFixed(2) + '%';

                    return [supportStr, votesStr, voterName || voter.slice(0, 20), delegate, percentage];
                }),
            );

            console.log(commentTable.toString());

            process.exit(0);
        });

    program
        .command('delegates')
        .description('Get list of delegates to address or ens')
        .argument('<address>', 'Address or ENS name to lookup for delegation')
        .action(async (address: string, cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
            const { rpc, disableTovarish } = options;
            const netId = RELAYER_NETWORK;

            const provider = await getProgramProvider(rpc, {
                netId,
                ...fetchDataOptions,
            });

            const config = getConfig(netId);

            const {
                governanceContract,
                aggregatorContract,
                reverseRecordsContract,
                constants: { GOVERNANCE_BLOCK },
            } = config;

            const tovarishClient = !disableTovarish
                ? (
                      await getTovarishRelayer({
                          options,
                          fetchDataOptions,
                          netId,
                      })
                  ).relayerClient
                : undefined;

            if (tovarishClient?.selectedRelayer) {
                console.log(`\nConnected with Tovarish Relayer ${tovarishClient.selectedRelayer.url}\n`);
            }

            const account = address.endsWith('.eth') ? await provider._getAddress(address) : getAddress(address);

            const governanceService = new NodeGovernanceService({
                netId,
                provider,
                Governance: Governance__factory.connect(governanceContract as string, provider),
                Aggregator: Aggregator__factory.connect(aggregatorContract as string, provider),
                ReverseRecords: ReverseRecords__factory.connect(reverseRecordsContract as string, provider),
                deployedBlock: GOVERNANCE_BLOCK,
                fetchDataOptions,
                tovarishClient,
                cacheDirectory: EVENTS_DIR,
                userDirectory: SAVED_DIR,
            });

            const { uniq, uniqNames, balances, balance } = await governanceService.getDelegatedBalance(account);

            const delegateTable = new Table();

            delegateTable.push(
                [{ colSpan: 2, content: 'Delegates', hAlign: 'center' }],
                ['Account', account.toLowerCase() !== address.toLowerCase() ? `${address} (${account})` : address],
                ['Delegated Balance', formatEther(balance) + ' TORN'],
                ...uniq.map((acc, index) => {
                    return [uniqNames[acc] || acc, formatEther(balances[index]) + ' TORN'];
                }),
            );

            console.log(delegateTable.toString());

            process.exit(0);
        });

    program
        .command('apy')
        .description('Calculate TORN Staking APY from last 30 days revenue')
        .action(async (cmdOptions: commonProgramOptions) => {
            const { options, fetchDataOptions } = await getProgramOptions(cmdOptions);
            const { rpc, disableTovarish } = options;
            const netId = RELAYER_NETWORK;

            const provider = await getProgramProvider(rpc, {
                netId,
                ...fetchDataOptions,
            });

            const config = getConfig(netId);

            const {
                tornContract,
                governanceContract,
                registryContract,
                constants: { REGISTRY_BLOCK },
            } = config;

            const tovarishClient = !disableTovarish
                ? (
                      await getTovarishRelayer({
                          options,
                          fetchDataOptions,
                          netId,
                      })
                  ).relayerClient
                : undefined;

            if (tovarishClient?.selectedRelayer) {
                console.log(`\nConnected with Tovarish Relayer ${tovarishClient.selectedRelayer.url}\n`);
            }

            const revenueService = new NodeRevenueService({
                netId,
                provider,
                RelayerRegistry: RelayerRegistry__factory.connect(registryContract as string, provider),
                deployedBlock: REGISTRY_BLOCK,
                fetchDataOptions,
                tovarishClient,
                cacheDirectory: EVENTS_DIR,
                userDirectory: SAVED_DIR,
            });

            const Governance = GovernanceVaultUpgrade__factory.connect(governanceContract as string, provider);

            const TORN = TORN__factory.connect(tornContract as string, provider);

            const [stakedBalance, { events }] = await Promise.all([
                Governance.userVault().then((vault) => TORN.balanceOf(vault)),
                revenueService.updateEvents(),
            ]);

            // Recent 30 days events
            const recentEvents = events
                .filter(({ timestamp }) => timestamp > Math.floor(Date.now() / 1000) - 30 * 86400)
                .reverse();

            const weeklyRevenue = recentEvents
                .filter(({ timestamp }) => timestamp > Math.floor(Date.now() / 1000) - 7 * 86400)
                .reduce((acc, { amountBurned }) => acc + BigInt(amountBurned), 0n);

            const monthlyRevenue = recentEvents.reduce((acc, { amountBurned }) => acc + BigInt(amountBurned), 0n);

            const weeklyYield = (Number(weeklyRevenue * 52n) / Number(stakedBalance)) * 100;

            const monthlyYield = (Number(monthlyRevenue * 12n) / Number(stakedBalance)) * 100;

            const statsTable = new Table();

            statsTable.push(
                [{ colSpan: 2, content: 'APY Stats', hAlign: 'center' }],
                [
                    'Yield (from 7d income):',
                    `${weeklyYield.toFixed(5)}% (${Number(formatEther(weeklyRevenue)).toFixed(3)} TORN)`,
                ],
                [
                    'Yield (from 30d income):',
                    `${monthlyYield.toFixed(5)}% (${Number(formatEther(monthlyRevenue)).toFixed(3)} TORN)`,
                ],
                ['Total Staked:', `${Number(formatEther(stakedBalance)).toFixed(3)} TORN`],
            );

            const apyTable = new Table();

            apyTable.push(
                [{ colSpan: 6, content: 'Recent Burns', hAlign: 'center' }],
                ['Block', 'Instance', 'Relayer', 'Relayer Fees', 'TORN Distributed', 'Timestamp'],
                ...recentEvents
                    .slice(0, 30)
                    .map(({ blockNumber, relayerAddress, amountBurned, instanceAddress, relayerFee, timestamp }) => {
                        const { amount, currency } = getInstanceByAddress(config, instanceAddress) as {
                            amount: string;
                            currency: string;
                        };

                        const { decimals } = config.tokens[currency];

                        return [
                            blockNumber,
                            `${amount} ${currency.toUpperCase()}`,
                            relayerAddress,
                            `${Number(formatUnits(relayerFee, decimals)).toFixed(5)} ${currency.toUpperCase()}`,
                            `${Number(formatEther(amountBurned)).toFixed(3)} TORN`,
                            moment.unix(timestamp).fromNow(),
                        ];
                    }),
            );

            console.log('\n\n' + statsTable.toString() + '\n');
            console.log('\n\n' + apyTable.toString() + '\n');

            process.exit(0);
        });

    // common options
    program.commands.forEach((cmd) => {
        cmd.option('-r, --rpc <RPC_URL>', 'The RPC that CLI should interact with', parseUrl);
        cmd.option('-e, --eth-rpc <ETHRPC_URL>', 'The Ethereum Mainnet RPC that CLI should interact with', parseUrl);
        cmd.option(
            '-a, --account-key <ACCOUNT_KEY>',
            'Account key generated from UI or the createAccount to store encrypted notes on-chain',
            parseRecoveryKey,
        );
        cmd.option('-R, --relayer <RELAYER>', 'Withdraw via relayer (Should be either .eth name or URL)', parseRelayer);
        cmd.option('-w, --wallet-withdrawal', 'Withdrawal via wallet (Should not be linked with deposits)');
        cmd.option('-T, --tor-port <TOR_PORT>', 'Optional tor port', parseNumber);
        cmd.option('-t, --token <TOKEN>', 'Token Contract address to view token balance', parseAddress);
        cmd.option(
            '-v, --view-only <VIEW_ONLY>',
            'Wallet address to view balance or to create unsigned transactions',
            parseAddress,
        );
        cmd.option(
            '-m, --mnemonic <MNEMONIC>',
            'Wallet BIP39 Mnemonic Phrase - If you did not add it to .env file and it is needed for operation',
            parseMnemonic,
        );
        cmd.option('-i, --mnemonic-index <MNEMONIC_INDEX>', 'Optional wallet mnemonic index', parseNumber);
        cmd.option(
            '-p, --private-key <PRIVATE_KEY>',
            'Wallet private key - If you did not add it to .env file and it is needed for operation',
            parseKey,
        );
        cmd.option(
            '-n, --non-interactive',
            'No confirmation mode - Does not show prompt for confirmation and allow to use scripts non-interactive',
        );
        cmd.option('-l, --local-rpc', 'Local node mode - Does not submit signed transaction to the node');
    });

    return program;
}
