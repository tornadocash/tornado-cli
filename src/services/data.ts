import path from 'path';
import { deflate, constants } from 'zlib';
import { stat, mkdir, readFile, writeFile } from 'fs/promises';
import { BaseEvents, CachedEvents, MinimalEvents, zipAsync, unzipAsync } from '@tornado/core';

export async function existsAsync(fileOrDir: string): Promise<boolean> {
    try {
        await stat(fileOrDir);

        return true;
    } catch {
        return false;
    }
}

/**
 * Supports legacy gz format for legacy UI
 */
export function deflateAsync(data: Uint8Array): Promise<Buffer> {
    return new Promise((resolve, reject) => {
        deflate(
            data,
            {
                level: constants.Z_BEST_COMPRESSION,
                strategy: constants.Z_FILTERED,
            },
            (err, buffer) => {
                if (!err) {
                    resolve(buffer);
                } else {
                    reject(err);
                }
            },
        );
    });
}

export async function saveLegacyFile({
    fileName,
    userDirectory,
    dataString,
}: {
    fileName: string;
    userDirectory: string;
    dataString: string;
}) {
    fileName = fileName.toLowerCase();

    const filePath = path.join(userDirectory, fileName);

    const payload = await deflateAsync(new TextEncoder().encode(dataString));

    if (!(await existsAsync(userDirectory))) {
        await mkdir(userDirectory, { recursive: true });
    }

    await writeFile(filePath + '.gz', payload);
}

export async function saveUserFile({
    fileName,
    userDirectory,
    dataString,
}: {
    fileName: string;
    userDirectory: string;
    dataString: string;
}) {
    fileName = fileName.toLowerCase();

    const filePath = path.join(userDirectory, fileName);

    const payload = await zipAsync({
        [fileName]: new TextEncoder().encode(dataString),
    });

    if (!(await existsAsync(userDirectory))) {
        await mkdir(userDirectory, { recursive: true });
    }

    await writeFile(filePath + '.zip', payload);
    await writeFile(filePath, dataString);
}

export async function loadSavedEvents<T extends MinimalEvents>({
    name,
    userDirectory,
}: {
    name: string;
    userDirectory: string;
}): Promise<BaseEvents<T>> {
    const filePath = path.join(userDirectory, `${name}.json`.toLowerCase());

    if (!(await existsAsync(filePath))) {
        return {
            events: [] as T[],
            lastBlock: 0,
        };
    }

    try {
        const events = JSON.parse(await readFile(filePath, { encoding: 'utf8' })) as T[];

        return {
            events,
            lastBlock: events[events.length - 1]?.blockNumber || 0,
        };
    } catch (err) {
        console.log('Method loadSavedEvents has error');
        console.log(err);
        return {
            events: [],
            lastBlock: 0,
        };
    }
}

export async function download({ name, cacheDirectory }: { name: string; cacheDirectory: string }) {
    const fileName = `${name}.json`.toLowerCase();
    const zipName = `${fileName}.zip`;
    const zipPath = path.join(cacheDirectory, zipName);

    const data = await readFile(zipPath);
    const { [fileName]: content } = await unzipAsync(data);

    return new TextDecoder().decode(content);
}

export async function loadCachedEvents<T extends MinimalEvents>({
    name,
    cacheDirectory,
    deployedBlock,
}: {
    name: string;
    cacheDirectory: string;
    deployedBlock: number;
}): Promise<CachedEvents<T>> {
    try {
        const module = await download({ cacheDirectory, name });

        if (module) {
            const events = JSON.parse(module);

            const lastBlock = events && events.length ? events[events.length - 1].blockNumber : deployedBlock;

            return {
                events,
                lastBlock,
                fromCache: true,
            };
        }

        return {
            events: [],
            lastBlock: deployedBlock,
            fromCache: true,
        };
    } catch (err) {
        console.log('Method loadCachedEvents has error');
        console.log(err);
        return {
            events: [],
            lastBlock: deployedBlock,
            fromCache: true,
        };
    }
}
