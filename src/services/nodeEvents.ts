import path from 'path';
import { readFile } from 'fs/promises';
import Table from 'cli-table3';
import moment from 'moment';
import {
    BatchBlockOnProgress,
    BatchEventOnProgress,
    BaseTornadoService,
    BaseEncryptedNotesService,
    BaseGovernanceService,
    BaseRegistryService,
    BaseRevenueService,
    BaseTornadoServiceConstructor,
    BaseEncryptedNotesServiceConstructor,
    BaseGovernanceServiceConstructor,
    BaseRegistryServiceConstructor,
    BaseRevenueServiceConstructor,
    BaseEchoServiceConstructor,
    BaseEchoService,
    CachedRelayers,
    toFixedHex,
    BaseEvents,
    DepositsEvents,
    WithdrawalsEvents,
    EncryptedNotesEvents,
    AllRelayerRegistryEvents,
    AllGovernanceEvents,
    EchoEvents,
    BaseEventsService,
    MinimalEvents,
    StakeBurnedEvents,
} from '@tornado/core';
import type { MerkleTree } from '@tornado/fixed-merkle-tree';
import { TreeCache } from './treeCache';
import { saveUserFile, loadSavedEvents, loadCachedEvents, existsAsync } from './data';

async function getEventsFromDB<T extends MinimalEvents>(service: BaseEventsService<T> & { userDirectory: string }) {
    if (!service.userDirectory) {
        console.log(`Updating ${service.getInstanceName()} events\n`);
        console.log(`savedEvents count - ${0}`);
        console.log(`savedEvents lastBlock - ${service.deployedBlock}\n`);

        return {
            events: [],
            lastBlock: 0,
        };
    }

    const savedEvents = await loadSavedEvents<T>({
        name: service.getInstanceName(),
        userDirectory: service.userDirectory,
    });

    console.log(`Updating ${service.getInstanceName()} events\n`);
    console.log(`savedEvents count - ${savedEvents.events.length}`);
    console.log(`savedEvents lastBlock - ${savedEvents.lastBlock}\n`);

    return savedEvents;
}

async function getEventsFromCache<T extends MinimalEvents>(service: BaseEventsService<T> & { cacheDirectory: string }) {
    if (!service.cacheDirectory) {
        console.log(`cachedEvents count - ${0}`);
        console.log(`cachedEvents lastBlock - ${service.deployedBlock}\n`);

        return {
            events: [],
            lastBlock: service.deployedBlock,
            fromCache: true,
        };
    }

    const cachedEvents = await loadCachedEvents<T>({
        name: service.getInstanceName(),
        cacheDirectory: service.cacheDirectory,
        deployedBlock: service.deployedBlock,
    });

    console.log(`cachedEvents count - ${cachedEvents.events.length}`);
    console.log(`cachedEvents lastBlock - ${cachedEvents.lastBlock}\n`);

    return cachedEvents;
}

async function saveEvents<T extends MinimalEvents>(
    service: BaseEventsService<T> & { userDirectory: string },
    { events, lastBlock }: BaseEvents<T>,
    eventTable?: Table.Table,
) {
    const instanceName = service.getInstanceName();

    console.log('\ntotalEvents count - ', events.length);
    console.log(
        `totalEvents lastBlock - ${events[events.length - 1] ? events[events.length - 1].blockNumber : lastBlock}\n`,
    );

    if (eventTable) {
        console.log(eventTable.toString() + '\n');
    }

    if (service.userDirectory) {
        await saveUserFile({
            fileName: instanceName + '.json',
            userDirectory: service.userDirectory,
            dataString: JSON.stringify(events, null, 2) + '\n',
        });
    }
}

export type NodeServiceConstructor = {
    cacheDirectory: string;
    userDirectory: string;
};

export type NodeTornadoServiceConstructor = BaseTornadoServiceConstructor &
    NodeServiceConstructor & {
        nativeCurrency: string;
        treeCache?: TreeCache;
    };

export class NodeTornadoService extends BaseTornadoService {
    cacheDirectory: string;
    userDirectory: string;
    nativeCurrency: string;

    treeCache?: TreeCache;

    constructor(serviceConstructor: NodeTornadoServiceConstructor) {
        super(serviceConstructor);

        const { cacheDirectory, userDirectory, nativeCurrency, treeCache } = serviceConstructor;

        this.cacheDirectory = cacheDirectory;
        this.userDirectory = userDirectory;
        this.nativeCurrency = nativeCurrency;

        this.treeCache = treeCache;
    }

    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]) {
        if (toBlock) {
            console.log(`fromBlock - ${fromBlock}`);
            console.log(`toBlock - ${toBlock}`);

            if (count) {
                console.log(`downloaded ${type} events count - ${count}`);
                console.log('____________________________________________');
                console.log(`Fetched ${type} events from ${fromBlock} to ${toBlock}\n`);
            }
        }
    }

    updateTransactionProgress({ currentIndex, totalIndex }: Parameters<BatchBlockOnProgress>[0]) {
        if (totalIndex) {
            console.log(`Fetched ${currentIndex} deposit txs of ${totalIndex}`);
        }
    }

    updateBlockProgress({ currentIndex, totalIndex }: Parameters<BatchBlockOnProgress>[0]) {
        if (totalIndex) {
            console.log(`Fetched ${currentIndex} withdrawal blocks of ${totalIndex}`);
        }
    }

    async getEventsFromDB() {
        return await getEventsFromDB<DepositsEvents | WithdrawalsEvents>(this);
    }

    async getEventsFromCache() {
        return await getEventsFromCache<DepositsEvents | WithdrawalsEvents>(this);
    }

    async validateEvents<S>({
        events,
        lastBlock,
        hasNewEvents,
    }: BaseEvents<DepositsEvents | WithdrawalsEvents> & {
        hasNewEvents?: boolean;
    }): Promise<S> {
        const tree = await super.validateEvents<S>({
            events,
            lastBlock,
            hasNewEvents,
        });

        if (tree && this.currency === this.nativeCurrency && this.treeCache) {
            const merkleTree = tree as unknown as MerkleTree;

            await this.treeCache.createTree(events as DepositsEvents[], merkleTree);

            console.log(
                `${this.getInstanceName()}: Updated tree cache with root ${toFixedHex(BigInt(merkleTree.root))}\n`,
            );
        }

        return tree;
    }

    async saveEvents({ events, lastBlock }: BaseEvents<DepositsEvents | WithdrawalsEvents>) {
        const eventTable = new Table();

        eventTable.push(
            [{ colSpan: 2, content: `${this.getType()}s`, hAlign: 'center' }],
            ['Instance', `${this.netId} chain ${this.amount} ${this.currency.toUpperCase()}`],
            ['Anonymity set', `${events.length} equal user ${this.getType().toLowerCase()}s`],
            [
                {
                    colSpan: 2,
                    content: `Latest ${this.getType().toLowerCase()}s`,
                },
            ],
            ...events
                .slice(events.length - 10)
                .reverse()
                .map(({ timestamp }, index) => {
                    const eventIndex = events.length - index;
                    const eventTime = moment.unix(timestamp).fromNow();

                    return [eventIndex, eventTime];
                }),
        );

        await saveEvents<DepositsEvents | WithdrawalsEvents>(this, { events, lastBlock }, eventTable);
    }
}

export type NodeEchoServiceConstructor = BaseEchoServiceConstructor & NodeServiceConstructor;

export class NodeEchoService extends BaseEchoService {
    cacheDirectory: string;
    userDirectory: string;

    constructor(serviceConstructor: NodeEchoServiceConstructor) {
        super(serviceConstructor);

        const { cacheDirectory, userDirectory } = serviceConstructor;

        this.cacheDirectory = cacheDirectory;
        this.userDirectory = userDirectory;
    }

    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]) {
        if (toBlock) {
            console.log(`fromBlock - ${fromBlock}`);
            console.log(`toBlock - ${toBlock}`);

            if (count) {
                console.log(`downloaded ${type} events count - ${count}`);
                console.log('____________________________________________');
                console.log(`Fetched ${type} events from ${fromBlock} to ${toBlock}\n`);
            }
        }
    }

    async getEventsFromDB() {
        return await getEventsFromDB<EchoEvents>(this);
    }

    async getEventsFromCache() {
        return await getEventsFromCache<EchoEvents>(this);
    }

    async saveEvents({ events, lastBlock }: BaseEvents<EchoEvents>) {
        const eventTable = new Table();

        eventTable.push(
            [{ colSpan: 2, content: 'Echo Accounts', hAlign: 'center' }],
            ['Network', `${this.netId} chain`],
            ['Events', `${events.length} events`],
            [{ colSpan: 2, content: 'Latest events' }],
            ...events
                .slice(events.length - 10)
                .reverse()
                .map(({ blockNumber }, index) => {
                    const eventIndex = events.length - index;

                    return [eventIndex, blockNumber];
                }),
        );

        await saveEvents<EchoEvents>(this, { events, lastBlock }, eventTable);
    }
}

export type NodeEncryptedNotesServiceConstructor = BaseEncryptedNotesServiceConstructor & NodeServiceConstructor;

export class NodeEncryptedNotesService extends BaseEncryptedNotesService {
    cacheDirectory: string;
    userDirectory: string;

    constructor(serviceConstructor: NodeEncryptedNotesServiceConstructor) {
        super(serviceConstructor);

        const { cacheDirectory, userDirectory } = serviceConstructor;

        this.cacheDirectory = cacheDirectory;
        this.userDirectory = userDirectory;
    }

    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]) {
        if (toBlock) {
            console.log(`fromBlock - ${fromBlock}`);
            console.log(`toBlock - ${toBlock}`);

            if (count) {
                console.log(`downloaded ${type} events count - ${count}`);
                console.log('____________________________________________');
                console.log(`Fetched ${type} events from ${fromBlock} to ${toBlock}\n`);
            }
        }
    }

    async getEventsFromDB() {
        return await getEventsFromDB<EncryptedNotesEvents>(this);
    }

    async getEventsFromCache() {
        return await getEventsFromCache<EncryptedNotesEvents>(this);
    }

    async saveEvents({ events, lastBlock }: BaseEvents<EncryptedNotesEvents>) {
        const eventTable = new Table();

        eventTable.push(
            [{ colSpan: 2, content: 'Encrypted Notes', hAlign: 'center' }],
            ['Network', `${this.netId} chain`],
            ['Events', `${events.length} events`],
            [{ colSpan: 2, content: 'Latest events' }],
            ...events
                .slice(events.length - 10)
                .reverse()
                .map(({ blockNumber }, index) => {
                    const eventIndex = events.length - index;

                    return [eventIndex, blockNumber];
                }),
        );

        await saveEvents<EncryptedNotesEvents>(this, { events, lastBlock }, eventTable);
    }
}

export type NodeGovernanceServiceConstructor = BaseGovernanceServiceConstructor & NodeServiceConstructor;

export class NodeGovernanceService extends BaseGovernanceService {
    cacheDirectory: string;
    userDirectory: string;

    constructor(serviceConstructor: NodeGovernanceServiceConstructor) {
        super(serviceConstructor);

        const { cacheDirectory, userDirectory } = serviceConstructor;

        this.cacheDirectory = cacheDirectory;
        this.userDirectory = userDirectory;
    }

    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]) {
        if (toBlock) {
            console.log(`fromBlock - ${fromBlock}`);
            console.log(`toBlock - ${toBlock}`);

            if (count) {
                console.log(`downloaded ${type} events count - ${count}`);
                console.log('____________________________________________');
                console.log(`Fetched ${type} events from ${fromBlock} to ${toBlock}\n`);
            }
        }
    }

    updateTransactionProgress({ currentIndex, totalIndex }: Parameters<BatchBlockOnProgress>[0]) {
        if (totalIndex) {
            console.log(`Fetched ${currentIndex} governance txs of ${totalIndex}`);
        }
    }

    async getEventsFromDB() {
        return await getEventsFromDB<AllGovernanceEvents>(this);
    }

    async getEventsFromCache() {
        return await getEventsFromCache<AllGovernanceEvents>(this);
    }

    async saveEvents({ events, lastBlock }: BaseEvents<AllGovernanceEvents>) {
        const eventTable = new Table();

        eventTable.push(
            [{ colSpan: 2, content: 'Governance Events', hAlign: 'center' }],
            ['Network', `${this.netId} chain`],
            ['Events', `${events.length} events`],
            [{ colSpan: 2, content: 'Latest events' }],
            ...events
                .slice(events.length - 10)
                .reverse()
                .map(({ blockNumber }, index) => {
                    const eventIndex = events.length - index;

                    return [eventIndex, blockNumber];
                }),
        );

        await saveEvents<AllGovernanceEvents>(this, { events, lastBlock }, eventTable);
    }
}

export type NodeRegistryServiceConstructor = BaseRegistryServiceConstructor & NodeServiceConstructor;

export class NodeRegistryService extends BaseRegistryService {
    cacheDirectory: string;
    userDirectory: string;

    constructor(serviceConstructor: NodeRegistryServiceConstructor) {
        super(serviceConstructor);

        const { cacheDirectory, userDirectory } = serviceConstructor;

        this.cacheDirectory = cacheDirectory;
        this.userDirectory = userDirectory;
    }

    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]) {
        if (toBlock) {
            console.log(`fromBlock - ${fromBlock}`);
            console.log(`toBlock - ${toBlock}`);

            if (count) {
                console.log(`downloaded ${type} events count - ${count}`);
                console.log('____________________________________________');
                console.log(`Fetched ${type} events from ${fromBlock} to ${toBlock}\n`);
            }
        }
    }

    async getEventsFromDB() {
        return await getEventsFromDB<AllRelayerRegistryEvents>(this);
    }

    async getEventsFromCache() {
        return await getEventsFromCache<AllRelayerRegistryEvents>(this);
    }

    async saveEvents({ events, lastBlock }: BaseEvents<AllRelayerRegistryEvents>) {
        const eventTable = new Table();

        eventTable.push(
            [{ colSpan: 2, content: 'Relayer Registry', hAlign: 'center' }],
            ['Network', `${this.netId} chain`],
            ['Events', `${events.length} events`],
            [{ colSpan: 2, content: 'Latest events' }],
            ...events
                .slice(events.length - 10)
                .reverse()
                .map(({ blockNumber }, index) => {
                    const eventIndex = events.length - index;

                    return [eventIndex, blockNumber];
                }),
        );

        await saveEvents<AllRelayerRegistryEvents>(this, { events, lastBlock }, eventTable);
    }

    async getRelayersFromDB(): Promise<CachedRelayers> {
        const filePath = path.join(this.userDirectory || '', 'relayers.json');

        if (!this.userDirectory || !(await existsAsync(filePath))) {
            return {
                lastBlock: 0,
                timestamp: 0,
                relayers: [],
            };
        }

        try {
            const { lastBlock, timestamp, relayers } = JSON.parse(await readFile(filePath, { encoding: 'utf8' }));

            return {
                lastBlock,
                timestamp,
                relayers,
            };
        } catch (err) {
            console.log('Method getRelayersFromDB has error');
            console.log(err);

            return {
                lastBlock: 0,
                timestamp: 0,
                relayers: [],
            };
        }
    }

    async getRelayersFromCache(): Promise<CachedRelayers> {
        const filePath = path.join(this.cacheDirectory || '', 'relayers.json');

        if (!this.cacheDirectory || !(await existsAsync(filePath))) {
            return {
                lastBlock: 0,
                timestamp: 0,
                relayers: [],
                fromCache: true,
            };
        }

        try {
            const { lastBlock, timestamp, relayers } = JSON.parse(await readFile(filePath, { encoding: 'utf8' }));

            return {
                lastBlock,
                timestamp,
                relayers,
                fromCache: true,
            };
        } catch (err) {
            console.log('Method getRelayersFromDB has error');
            console.log(err);

            return {
                lastBlock: 0,
                timestamp: 0,
                relayers: [],
                fromCache: true,
            };
        }
    }

    async saveRelayers({ lastBlock, timestamp, relayers }: CachedRelayers) {
        if (this.userDirectory) {
            await saveUserFile({
                fileName: 'relayers.json',
                userDirectory: this.userDirectory,
                dataString: JSON.stringify({ lastBlock, timestamp, relayers }, null, 2) + '\n',
            });
        }
    }
}

export type NodeRevenueServiceConstructor = BaseRevenueServiceConstructor & NodeServiceConstructor;

export class NodeRevenueService extends BaseRevenueService {
    cacheDirectory: string;
    userDirectory: string;

    constructor(serviceConstructor: NodeRevenueServiceConstructor) {
        super(serviceConstructor);

        const { cacheDirectory, userDirectory } = serviceConstructor;

        this.cacheDirectory = cacheDirectory;
        this.userDirectory = userDirectory;
    }

    updateEventProgress({ type, fromBlock, toBlock, count }: Parameters<BatchEventOnProgress>[0]) {
        if (toBlock) {
            console.log(`fromBlock - ${fromBlock}`);
            console.log(`toBlock - ${toBlock}`);

            if (count) {
                console.log(`downloaded ${type} events count - ${count}`);
                console.log('____________________________________________');
                console.log(`Fetched ${type} events from ${fromBlock} to ${toBlock}\n`);
            }
        }
    }

    updateTransactionProgress({ currentIndex, totalIndex }: Parameters<BatchBlockOnProgress>[0]) {
        if (totalIndex) {
            console.log(`Fetched ${currentIndex} txs of ${totalIndex}`);
        }
    }

    updateBlockProgress({ currentIndex, totalIndex }: Parameters<BatchBlockOnProgress>[0]) {
        if (totalIndex) {
            console.log(`Fetched ${currentIndex} blocks of ${totalIndex}`);
        }
    }

    async getEventsFromDB() {
        return await getEventsFromDB<StakeBurnedEvents>(this);
    }

    async getEventsFromCache() {
        return await getEventsFromCache<StakeBurnedEvents>(this);
    }

    async saveEvents({ events, lastBlock }: BaseEvents<StakeBurnedEvents>) {
        const eventTable = new Table();

        eventTable.push(
            [{ colSpan: 2, content: 'Stake Burned', hAlign: 'center' }],
            ['Network', `${this.netId} chain`],
            ['Events', `${events.length} events`],
            [{ colSpan: 2, content: 'Latest events' }],
            ...events
                .slice(events.length - 10)
                .reverse()
                .map(({ blockNumber }, index) => {
                    const eventIndex = events.length - index;

                    return [eventIndex, blockNumber];
                }),
        );

        await saveEvents<StakeBurnedEvents>(this, { events, lastBlock }, eventTable);
    }
}
