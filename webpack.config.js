const path = require('path');
const { BannerPlugin } = require('webpack');

const esbuildLoader = {
    test: /\.ts?$/,
    loader: 'esbuild-loader',
    options: {
        loader: 'ts',
        target: 'es2022',
    }
}

module.exports = [
    {
        mode: 'production',
        module: {
            rules: [esbuildLoader]
        },
        entry: './src/cli.ts',
        output: {
            filename: 'cli.js',
            path: path.resolve(__dirname, './dist'),
        },
        target: 'node',
        plugins: [
            new BannerPlugin({
                banner: '#!/usr/bin/env node\n',
                raw: true
            })
        ],
        resolve: {
            extensions: ['.tsx', '.ts', '.js'],
            alias: {
                'fflate': 'fflate/node'
            },
            fallback: {
                '@tornado/websnark/src/utils': '@tornado/websnark/src/utils.js',
                '@tornado/websnark/src/groth16': '@tornado/websnark/src/groth16.js',
            }
        },
        optimization: {
            minimize: false,
        }
    }
];
